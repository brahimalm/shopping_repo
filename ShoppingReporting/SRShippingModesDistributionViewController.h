//
//  SRPaymentModesPieChart.h
//  ShoppingReporting
//
//  Created by Boniface Mathieu on 5/28/13.
//  Copyright (c) 2013 Boniface Mathieu. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SRShippingModesDistributionViewController : UIViewController<CPTPlotDataSource, UIActionSheetDelegate>

- (void) reloadData:(NSDictionary *)newValues;

-(void) noData;

-(void) errorFetchingData;

@end
