//
//  SRTop10.m
//  ShoppingReporting
//
//  Created by Mathieu Boniface on 06/08/13.
//  Copyright (c) 2013 Boniface Mathieu. All rights reserved.
//

#import "SRTop10ViewController.h"
#import "ProductCell.h"
#import <SDWebImage/UIImageView+WebCache.h>

@interface SRTop10ViewController ()

@property (weak, nonatomic) IBOutlet UILabel *noDataToDisplayLabel;
@property (weak, nonatomic) IBOutlet UITableView *hostView;

@property (nonatomic, strong) NSArray *top10;

@end

@implementation SRTop10ViewController

@synthesize noDataToDisplayLabel;
@synthesize hostView;
@synthesize top10;

-(void) viewDidLoad {
    hostView.backgroundView = nil;
    hostView.backgroundColor = [UIColor clearColor];
    hostView.separatorStyle = UITableViewCellSeparatorStyleNone;
}

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [top10 count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    //Where we configure the cell in each row
    
    static NSString *CellIdentifier = @"Cell";
    ProductCell *cell;
    
    cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[ProductCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    NSDictionary *product = [top10 objectAtIndex:indexPath.row];

    [cell.productLabel setText:[product objectForKey:@"label"]];

    NSString *productRef = [NSString stringWithFormat:@"%@", [product objectForKey:@"reference"]];
    [cell.productRef setText:productRef];
    
    NSString *format;
    if ([[product objectForKey:@"quantity"] intValue] > 1) {
        format = @"%@ vendus";
    } else {
        format = @"%@ vendu";
    }
    
    NSString *salesQuantity = [NSString stringWithFormat:format, [product objectForKey:@"quantity"]];
    [cell.productSalesQuantity setText:salesQuantity];
    
    NSString * imageURL = [product objectForKey:@"imageURL"];
    if (imageURL != (id)[NSNull null]) {
        [cell.productImage setImageWithURL:[NSURL URLWithString:imageURL]];
    } else {
        [cell.productImage setImage:[UIImage imageNamed:@"no-image.png"]];
    }

    cell.selectionStyle = UITableViewCellSelectionStyleNone ;

    return cell;
}

#pragma mark - Table view delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSDictionary *product = [top10 objectAtIndex:indexPath.row];

    NSString * url = [product objectForKey:@"productURL"];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
}

- (void) reloadDataWithTop10: (NSArray*)newValues {
    if (newValues != nil && [newValues count] > 0) {
        top10 = newValues;
        [noDataToDisplayLabel setHidden:YES];
        [hostView setHidden:NO];
        [hostView reloadData];
    } else {
        [self errorFetchingData];
    }
}

-(void) errorFetchingData {
    [hostView setHidden:YES];
    [noDataToDisplayLabel setTextColor:[SRConstants unableToFetchDataLabelColor]];
    [noDataToDisplayLabel setText:SRUnableToFetchDataText];
    [noDataToDisplayLabel setHidden:NO];
}

-(void) noData {
    [hostView setHidden:YES];
    [noDataToDisplayLabel setTextColor:[SRConstants noDataLabelColor]];
    [noDataToDisplayLabel setText:SRNoDataText];
    [noDataToDisplayLabel setHidden:NO];
}

#pragma mark - Rotation
-(BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    return (interfaceOrientation == UIInterfaceOrientationLandscapeLeft);
}

- (void)viewDidUnload {
    [self setHostView:nil];
    [super viewDidUnload];
}

@end
