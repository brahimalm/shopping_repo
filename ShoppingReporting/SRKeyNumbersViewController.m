//
//  SRKeyNumbersViewController.m
//  ShoppingReporting
//
//  Created by Boniface Mathieu on 5/29/13.
//  Copyright (c) 2013 Boniface Mathieu. All rights reserved.
//

#import "SRKeyNumbersViewController.h"

@interface SRKeyNumbersViewController ()
@property (weak, nonatomic)     IBOutlet UILabel    *turnOverLabel;
@property (weak, nonatomic)     IBOutlet UILabel    *numProductsByOrderLabel;
@property (weak, nonatomic)     IBOutlet UILabel    *numOrdersLabel;
@property (weak, nonatomic)     IBOutlet UILabel    *numProductsSoldLabel;
@property (weak, nonatomic)     IBOutlet UILabel    *amountByOrderLabel;

@property (weak, nonatomic)     IBOutlet UILabel    *dateLabel;

@property (weak, nonatomic)     IBOutlet UIButton   *rewardButton;
@property (weak, nonatomic)     IBOutlet UIButton   *forwardButton;

@property (weak, nonatomic)     IBOutlet UILabel    *noDataToDisplayLabel;

@property (weak, nonatomic)     IBOutlet UIView     *hostView;

@end

@implementation SRKeyNumbersViewController

@synthesize delegate;
@synthesize hostView;
@synthesize noDataToDisplayLabel;

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) reloadDataWithDataWithTurnOver:(NSString *) turnOver //
                  andNumProductsByOrder:(NSString *) numProductsByOrder //
                           andNumOrders:(NSString *) numOrders //
                     andNumProductsSold:(NSString *) numProductsSold //
                       andAmountByOrder:(NSString *) amountByOrder {
    
    if (turnOver == nil //
        && numProductsByOrder == nil //
        && numOrders == nil //
        && numProductsSold == nil //
        && amountByOrder == nil) {
        
        [self errorFetchingData];
        
    } else {
        [hostView setHidden:NO];
        [noDataToDisplayLabel setHidden:YES];

    }

    [self.turnOverLabel setText:turnOver];
    [self.numProductsByOrderLabel setText:numProductsByOrder];
    [self.numOrdersLabel setText:numOrders];
    [self.numProductsSoldLabel setText:numProductsSold];
    [self.amountByOrderLabel setText:amountByOrder];
}

-(void) noData {
    [self emptyValuesLabels];
    [noDataToDisplayLabel setTextColor:[SRConstants noDataLabelColor]];
    [noDataToDisplayLabel setText:SRNoDataText];
    [hostView setHidden:YES];
    [noDataToDisplayLabel setHidden:NO];
}

-(void) errorFetchingData {
    [self emptyValuesLabels];
    [noDataToDisplayLabel setTextColor:[SRConstants unableToFetchDataLabelColor]];
    [noDataToDisplayLabel setText:SRUnableToFetchDataText];
    [hostView setHidden:YES];
    [noDataToDisplayLabel setHidden:NO];
}

-(void) emptyValuesLabels {
    [self.turnOverLabel setText:@""];
    [self.numProductsByOrderLabel setText:@""];
    [self.numOrdersLabel setText:@""];
    [self.numProductsSoldLabel setText:@""];
    [self.amountByOrderLabel setText:@""];
}


-(void) updateDateLabelWithDate:(NSDate *) date andPeriod:(int) period {
    NSString *format;
    NSString *text;
    
    if (period == SRPeriodMonth) {
        format = @"MMM yyyy";
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:format];
        
        text = [formatter stringFromDate:date];
    } else if (period == SRPeriodWeek) {
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"dd-MM-yyyy"];
        NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
        NSDateComponents *comp = [gregorian components:NSWeekCalendarUnit fromDate:date];
        
        text =[NSString stringWithFormat:@"Semaine %d", comp.week];
    } else if (period == SRPeriodDay) {
        format = @"dd MMM yyyy";
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:format];
        
        text = [formatter stringFromDate:date];
    }
    [self.dateLabel setText:text];
}

- (IBAction)rewardButtonClicked:(id)sender {
    [delegate onTimeOffsetChanged:+1];
}

- (IBAction)forwardButtonClicked:(id)sender {
    [delegate onTimeOffsetChanged:-1];
}

-(void) setForwardButtonEnabled:(BOOL) enabled {
    [self.forwardButton setEnabled:enabled];
}

-(void) setRewardButtonEnabled:(BOOL) enabled {
    [self.rewardButton setEnabled:enabled];
}

- (void)viewDidUnload {
    [self setRewardButton:nil];
    [self setForwardButton:nil];
    [self setNoDataToDisplayLabel:nil];
    [self setHostView:nil];
    [super viewDidUnload];
}
@end
