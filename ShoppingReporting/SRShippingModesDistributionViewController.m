//
//  SRPaymentModesPieChart.m
//  ShoppingReporting
//
//  Created by Boniface Mathieu on 5/28/13.
//  Copyright (c) 2013 Boniface Mathieu. All rights reserved.
//

#import "SRShippingModesDistributionViewController.h"

@interface SRShippingModesDistributionViewController ()

@property (strong, nonatomic) IBOutlet CPTGraphHostingView *hostView;
@property (nonatomic, strong) CPTPieChart *numCommandsPieChart;
@property (nonatomic, strong) NSDictionary *numCommandsPerPaymentMode;
@property (nonatomic, strong) NSMutableArray *availableShippingModes;
@property (nonatomic, strong) NSDictionary *shippingModesLabels;
@property (nonatomic, strong) NSDictionary *shippingModesColors;
@property (weak, nonatomic) IBOutlet UILabel *noDataToDisplayLabel;

@end

@implementation SRShippingModesDistributionViewController

@synthesize numCommandsPieChart;
@synthesize numCommandsPerPaymentMode;
@synthesize availableShippingModes;
@synthesize noDataToDisplayLabel;
@synthesize hostView;
@synthesize shippingModesLabels;
@synthesize shippingModesColors;


#pragma mark - CPTPlotDataSource methods
-(NSUInteger)numberOfRecordsForPlot:(CPTPlot *)plot {
    return [numCommandsPerPaymentMode count];
}

-(void) viewDidLoad {
    availableShippingModes = [[NSMutableArray alloc] initWithCapacity:6];
    
    shippingModesLabels = [[NSDictionary alloc] initWithObjectsAndKeys://
                           @"Livraison à domicile", SRLADShippingModeKey, //
                           @"Liv. dir. distrib. Small", SRLDDSmallShippingModeKey, //
                           @"Liv. Dir. Distrib. XL", SRLDDXlShippingModeKey, //
                           @"Retrait Express", SRREShippingModeKey, //
                           @"Recommandé A/R", SRRAShippingModeKey, //
                           nil];
    
    CPTColor *RAColor = [[CPTColor alloc]
                         initWithComponentRed:  (float)0x6B / 0xFF // VIOLET
                         green:                 (float)0x21 / 0xFF
                         blue:                  (float)0x9E / 0xFF
                         alpha:                 (float)0xFF / 0xFF];
    CPTColor *LDDXlColor = [[CPTColor alloc]
                            initWithComponentRed:  (float)0x92 / 0xFF // VERT
                            green:                 (float)0xCE / 0xFF
                            blue:                  (float)0x14 / 0xFF
                            alpha:                 (float)0xFF / 0xFF];
    CPTColor *LDDSmallColor = [[CPTColor alloc]
                               initWithComponentRed:  (float)0xB7 / 0xFF // ROUGE
                               green:                 (float)0x22 / 0xFF
                               blue:                  (float)0x1C / 0xFF
                               alpha:                 (float)0xFF / 0xFF];
    CPTColor *REColor = [[CPTColor alloc]
                         initWithComponentRed:  (float)0xEE / 0xFF // ORANGE
                         green:                 (float)0x87 / 0xFF
                         blue:                  (float)0x06 / 0xFF
                         alpha:                 (float)0xFF / 0xFF];
    CPTColor *LADColor = [[CPTColor alloc]
                          initWithComponentRed:  (float)0x64 / 0xFF // BLEU
                          green:                 (float)0x9E / 0xFF
                          blue:                  (float)0x90 / 0xFF
                          alpha:                 (float)0xFF / 0xFF];
    
    shippingModesColors = [[NSMutableDictionary alloc] initWithObjectsAndKeys://
                           LADColor, SRLADShippingModeKey,//
                           LDDSmallColor, SRLDDSmallShippingModeKey, //
                           LDDXlColor, SRLDDXlShippingModeKey, //
                           REColor, SRREShippingModeKey, //
                           RAColor, SRRAShippingModeKey, //
                           nil];
    
    [self initPlot];
}

-(NSString *)legendTitleForPieChart:(CPTPieChart *)pieChart recordIndex:(NSUInteger)index {
    return [shippingModesLabels objectForKey:[availableShippingModes objectAtIndex:index]];
}

-(CPTFill *)sliceFillForPieChart:(CPTPieChart *)pieChart recordIndex:(NSUInteger)index {
    return [CPTFill fillWithColor:[shippingModesColors objectForKey:[availableShippingModes objectAtIndex:index]]];
}


-(NSNumber *)numberForPlot:(CPTPlot *)plot field:(NSUInteger)fieldEnum recordIndex:(NSUInteger)index {
    return [numCommandsPerPaymentMode objectForKey:[availableShippingModes objectAtIndex:index]];
}

-(CPTLayer *)dataLabelForPlot:(CPTPlot *)plot recordIndex:(NSUInteger)index {

    static CPTMutableTextStyle *labelText = nil;
    double value;
    double valuesSum = 0;
    double percent = 0;
    
    labelText= [[CPTMutableTextStyle alloc] init];
    CPTColor * color = [shippingModesColors objectForKey:[availableShippingModes objectAtIndex:index]];
    
    labelText.color = color;
    
    NSString *labelValue = nil;
   
    value = [[numCommandsPerPaymentMode objectForKey:[availableShippingModes objectAtIndex:index]] doubleValue];


    for (NSString* key in availableShippingModes) {
        valuesSum += [[numCommandsPerPaymentMode objectForKey:key] doubleValue];
    }

    percent = value / valuesSum;
    
    if (index == 0) {
        double angle = (percent) * 2 * M_PI;
        double startAngle = 5 * M_PI_4 / 4 + angle / 2;
        numCommandsPieChart.startAngle = startAngle;
    }
    
    if (value != 0) {
        labelValue = [NSString stringWithFormat:@"%0.0f (%0.1f %%)", value, percent * 100.0f];
    } else {
        labelValue = @"";
    }

    CPTTextLayer *labelLayer = [[CPTTextLayer alloc] initWithText:labelValue style:labelText];
    plot.labelOffset = 10;
    return labelLayer;
}

#pragma mark - Rotation
-(BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    return (interfaceOrientation == UIInterfaceOrientationLandscapeLeft);
}

#pragma mark - Chart behavior
-(void)initPlot {
    
    self.hostView.allowPinchScaling = NO;
    
    [self configureGraph];
    [self configureChart];
    [self configureLegend];
}

-(void) configureLegend {
    CPTGraph *graph = self.hostView.hostedGraph;
    CPTLegend *theLegend = [CPTLegend legendWithGraph:graph];

    theLegend.numberOfColumns = 2;

    CPTMutableTextStyle *legendTextStyle = [[CPTMutableTextStyle alloc] init];
    [legendTextStyle setColor:[CPTColor whiteColor]];

    theLegend.textStyle = legendTextStyle;
    
    theLegend.fill = [CPTFill fillWithColor:[CPTColor grayColor]];
    theLegend.cornerRadius = 5.0;

    graph.legend = theLegend;
    graph.legendAnchor = CPTRectAnchorBottom;
    graph.legendDisplacement = CGPointMake(0, 5);
}

-(void)configureGraph {
    
    CPTGraph *graph = [[CPTXYGraph alloc] init];
    self.hostView.hostedGraph = graph;
    
    [graph applyTheme:[CPTTheme themeNamed:kCPTDarkGradientTheme]];
    graph.plotAreaFrame.cornerRadius = 0;
    
    graph.fill = [CPTFill fillWithColor:[CPTColor clearColor]];
    graph.plotAreaFrame.fill = [CPTFill fillWithColor:[CPTColor clearColor]];
    
    graph.paddingBottom = 3.0f;
    graph.paddingLeft  = 3.0f;
    graph.paddingTop    = 0.0f;
    graph.paddingRight  = 3.0f;
    graph.axisSet = nil;
    
    CPTMutableTextStyle *titleStyle = [CPTMutableTextStyle textStyle];
    titleStyle.color = [CPTColor darkGrayColor];
    titleStyle.fontName = @"HelveticaNeue-Light";
    titleStyle.fontSize = 16.0f;
    titleStyle.textAlignment = CPTTextAlignmentCenter;
    
    NSString *title = @"Activité LM.fr \nRépartition du nombre de commandes \npar groupe de livraison";
    graph.title = title;
    graph.titleTextStyle = titleStyle;
    graph.titlePlotAreaFrameAnchor = CPTRectAnchorTop;
    graph.titleDisplacement = CGPointMake(0, -3.0f);
    
    graph.plotAreaFrame.borderLineStyle = nil;
    graph.plotAreaFrame.paddingBottom = 5.0f;
    graph.plotAreaFrame.paddingLeft  = 5.0f;
    graph.plotAreaFrame.paddingTop    = 0.0f;
    graph.plotAreaFrame.paddingRight  = 5.0f;
}

-(void)configureChart {
    
    CPTGraph *graph = self.hostView.hostedGraph;
    numCommandsPieChart = [[CPTPieChart alloc] init];
    numCommandsPieChart.dataSource = self;
    numCommandsPieChart.delegate = self;
    numCommandsPieChart.pieRadius = 90;
    numCommandsPieChart.sliceDirection = CPTPieDirectionClockwise;
    numCommandsPieChart.centerAnchor = CGPointMake(0.5, 0.5);
    
    CPTGradient *numCommandsOverlayGradient = [[CPTGradient alloc] init];
    numCommandsOverlayGradient.gradientType = CPTGradientTypeRadial;
    
    CPTColor *numCommandsGradientColor = [CPTColor darkGrayColor];
    
    numCommandsOverlayGradient = [numCommandsOverlayGradient addColorStop:[numCommandsGradientColor colorWithAlphaComponent:0.0] atPosition:0.5];
    numCommandsOverlayGradient = [numCommandsOverlayGradient addColorStop:[numCommandsGradientColor colorWithAlphaComponent:0.4] atPosition:1.0];
    numCommandsPieChart.overlayFill = [CPTFill fillWithGradient:numCommandsOverlayGradient];
    [graph addPlot:numCommandsPieChart];
    
}

- (void) reloadData:(NSDictionary *)newValues {
    [availableShippingModes removeAllObjects];
    numCommandsPerPaymentMode = newValues;
    
    for (NSString* key in numCommandsPerPaymentMode) {
        [availableShippingModes addObject:key];
    }
    
    if (newValues != nil && [newValues count] > 0) {
        [numCommandsPieChart reloadData];
        [self configureLegend];
        
        [noDataToDisplayLabel setHidden:YES];
        [hostView setHidden:NO];

    } else {

        [self errorFetchingData];
    }
}

-(void) noData {
    [hostView setHidden:YES];
    [noDataToDisplayLabel setTextColor:[SRConstants noDataLabelColor]];
    [noDataToDisplayLabel setText:SRNoDataText];
    [noDataToDisplayLabel setHidden:NO];
}

-(void) errorFetchingData {
    [hostView setHidden:YES];
    [noDataToDisplayLabel setTextColor:[SRConstants unableToFetchDataLabelColor]];
    [noDataToDisplayLabel setText:SRUnableToFetchDataText];
    [noDataToDisplayLabel setHidden:NO];
}

- (void)viewDidUnload {
    [self setNoDataToDisplayLabel:nil];
    [super viewDidUnload];
}
@end
