//
//  SPFirstViewController.h
//  ShoppingReporting
//
//  Created by Boniface Mathieu on 5/3/13.
//  Copyright (c) 2013 Boniface Mathieu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SRWebAndStoreViewController : UIViewController<CPTPlotDataSource, UIActionSheetDelegate>

-(void) reloadDataWithData:(NSDictionary *)newValues;

-(void) noData;

-(void) errorFetchingData;

@end
