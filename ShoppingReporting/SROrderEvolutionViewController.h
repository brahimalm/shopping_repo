//
//  SROrderEvolutionViewController.h
//  ShoppingReporting
//
//  Created by Mathieu Boniface on 09/08/13.
//  Copyright (c) 2013 Boniface Mathieu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SROrderEvolutionViewController : UIViewController<CPTPlotDataSource, CPTPlotSpaceDelegate>

-(void) reloadDataWithBeginDate:            (NSTimeInterval)        beginDate
                     andEndDate:            (NSTimeInterval)        endDate
                     andResults:            (NSArray *)  results
                     andBeginDatePrevious:  (NSTimeInterval)        beginDatePrevious
                     andEndDatePrevious:    (NSTimeInterval)        endDatePrevious
                     andPreviousResults:    (NSArray *)  previousResults
                     andSelectedPeriod:     (int)        selectedPeriod;

-(void) errorFetchingData;
-(void) noData;

@end
