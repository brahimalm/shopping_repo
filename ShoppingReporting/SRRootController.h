//
//  SRGraphController.h
//  ShoppingReporting
//
//  Created by Boniface Mathieu on 5/17/13.
//  Copyright (c) 2013 Boniface Mathieu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SRKeyNumbersViewController.h"

@interface SRRootController : UIViewController<NSURLConnectionDelegate, UIScrollViewDelegate, SRkeyNumbersDelegate>

-(void) refreshAppData;

@end
