//
//  main.m
//  ShoppingReporting
//
//  Created by Boniface Mathieu on 5/3/13.
//  Copyright (c) 2013 Boniface Mathieu. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "SRAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([SRAppDelegate class]));
    }
}
