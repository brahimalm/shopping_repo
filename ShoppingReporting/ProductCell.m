//
//  ProductCell.m
//  ShoppingReporting
//
//  Created by Mathieu Boniface on 06/08/13.
//  Copyright (c) 2013 Boniface Mathieu. All rights reserved.
//

#import "ProductCell.h"

@interface ProductCell ()

@end

@implementation ProductCell

@synthesize productImage;
@synthesize hostView;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
    }
    return self;
}

-(void)layoutSubviews {
    [super layoutSubviews];

    [productImage.layer setBorderColor:[UIColor grayColor].CGColor];
    [productImage.layer setBorderWidth:1.0f];

    [productImage.layer setShadowOpacity:0.8];
    [productImage.layer setShadowRadius:2.0];
    [productImage.layer setShadowOffset:CGSizeMake(1.5, 1.5)];

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

@end
