//
//  SRTop10.h
//  ShoppingReporting
//
//  Created by Mathieu Boniface on 06/08/13.
//  Copyright (c) 2013 Boniface Mathieu. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SRTop10ViewController : UIViewController<UITableViewDataSource, UITableViewDelegate>

-(void) reloadDataWithTop10: (NSArray*)top10;

-(void) errorFetchingData;
-(void) noData;

@end
