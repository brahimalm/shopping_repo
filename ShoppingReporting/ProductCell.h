//
//  ProductCell.h
//  ShoppingReporting
//
//  Created by Mathieu Boniface on 06/08/13.
//  Copyright (c) 2013 Boniface Mathieu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProductCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *productImage;
@property (weak, nonatomic) IBOutlet UILabel *productLabel;
@property (weak, nonatomic) IBOutlet UILabel *productRef;
@property (weak, nonatomic) IBOutlet UIView  *hostView;
@property (weak, nonatomic) IBOutlet UILabel *productSalesQuantity;

@end
