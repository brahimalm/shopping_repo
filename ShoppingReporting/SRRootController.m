//
//  SRGraphController.m
//  ShoppingReporting
//
//  Created by Boniface Mathieu on 5/17/13.
//  Copyright (c) 2013 Boniface Mathieu. All rights reserved.
//

#import "SRRootController.h"
#import "SVProgressHUD.h"
#import "SRWebAndStoreViewController.h"
#import "SRPaymentModesViewController.h"
#import "SRKeyNumbersViewController.h"
#import "SRShippingModesDistributionViewController.h"
#import "SRTop10ViewController.h"
#import "SROrderEvolutionViewController.h"

#define SECOND_IN_A_DAY 60 * 60 * 24
#define SECOND_IN_A_WEEK SECOND_IN_A_DAY * 7

@interface SRRootController ()

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UISegmentedControl *periodSegmentControl;
@property (weak, nonatomic) IBOutlet UISegmentedControl *screenSegmentControl;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;

@property (nonatomic, strong) SRWebAndStoreViewController *webAndStoreViewController;
@property (nonatomic, strong) SRKeyNumbersViewController *keyNumbersViewController;
@property (nonatomic, strong) SRPaymentModesViewController *paymentModesViewController;
@property (nonatomic, strong) SRShippingModesDistributionViewController *shippingModesDistributionViewController;
@property (nonatomic, strong) SRTop10ViewController *top10ViewController;
@property (nonatomic, strong) SROrderEvolutionViewController *orderEvolutionViewController;


// Objects related to network tasks for each Rest API Methods
@property (nonatomic, strong)   NSMutableData     *ordersRequestReceivedData;
@property (nonatomic, strong)   NSURLConnection   *ordersRequestConnection;
@property (nonatomic)           NSInteger          ordersRequestStatus;

@property (nonatomic, strong)   NSMutableData     *shippingModesReceivedData;
@property (nonatomic, strong)   NSURLConnection   *shippingModesRequestConnection;
@property (nonatomic)           int                shippingModesRequestStatus;

@property (nonatomic, strong)   NSMutableData     *paymentModesReceivedData;
@property (nonatomic, strong)   NSURLConnection   *paymentModesRequestConnection;
@property (nonatomic)           int                paymentModesRequestStatus;

@property (nonatomic, strong)   NSMutableData     *keyNumbersReceivedData;
@property (nonatomic, strong)   NSURLConnection   *keyNumbersRequestConnection;
@property (nonatomic)           int                keyNumbersRequestStatus;

@property (nonatomic, strong)   NSMutableData     *top10ReceivedData;
@property (nonatomic, strong)   NSURLConnection   *top10RequestConnection;
@property (nonatomic)           int                top10RequestStatus;

@property (nonatomic, strong)   NSMutableData     *orderEvolutionReceivedData;
@property (nonatomic, strong)   NSURLConnection   *orderEvolutionRequestConnection;
@property (nonatomic)           int                orderEvolutionRequestStatus;

@property (nonatomic) int timeOffset;

@property (weak, nonatomic) IBOutlet UIBarButtonItem *rewardButton;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *forwardButton;

@property (strong, nonatomic) NSString *endpoint;


@end

@implementation SRRootController

@synthesize ordersRequestReceivedData;
@synthesize ordersRequestConnection;
@synthesize ordersRequestStatus;
@synthesize paymentModesReceivedData;
@synthesize paymentModesRequestConnection;
@synthesize paymentModesRequestStatus;
@synthesize shippingModesReceivedData;
@synthesize shippingModesRequestConnection;
@synthesize shippingModesRequestStatus;
@synthesize keyNumbersReceivedData;
@synthesize keyNumbersRequestConnection;
@synthesize keyNumbersRequestStatus;
@synthesize top10ReceivedData;
@synthesize top10RequestConnection;
@synthesize top10RequestStatus;
@synthesize orderEvolutionReceivedData;
@synthesize orderEvolutionRequestConnection;
@synthesize orderEvolutionRequestStatus;

int const numScreens = 6;

@synthesize rewardButton;
@synthesize forwardButton;

@synthesize timeOffset;

CGFloat const SRBarWidth = 0.40f;
CGFloat const SRBarInitialX = 0.25f;

BOOL pageControlUsed = NO;

#pragma mark - UIViewController lifecycle methods
-(void)viewDidLayoutSubviews {
    
    [super viewDidLayoutSubviews];

    self.keyNumbersViewController = [self instanciateAndPushViewControllerWithId:@"KeyNumbersController" toPageIndex:0];
    [self.keyNumbersViewController setDelegate:self];
    self.webAndStoreViewController  = [self instanciateAndPushViewControllerWithId:@"WebAndStorePieChartController" toPageIndex:1];
    self.shippingModesDistributionViewController = //
        [self instanciateAndPushViewControllerWithId:@"ShippingModesDistributionController" toPageIndex:2];
    self.paymentModesViewController = [self instanciateAndPushViewControllerWithId:@"PaymentModesPieChartController" toPageIndex:3];
    self.top10ViewController = [self instanciateAndPushViewControllerWithId:@"Top10Controller" toPageIndex:4];
    self.orderEvolutionViewController = [self instanciateAndPushViewControllerWithId:@"OrderEvolutionController" toPageIndex:5];
    
    self.scrollView.contentSize = CGSizeMake( //
                                             self.scrollView.frame.size.width * numScreens, //
                                             self.scrollView.frame.size.height);
    [self.view layoutSubviews];
    [self.scrollView setDelegate:self];
    
    CGRect initialViewPort = CGRectMake(//
                                   0, //
                                   0, //
                                   self.scrollView.frame.size.width, //
                                   self.scrollView.frame.size.height);
    
    [self.scrollView scrollRectToVisible:initialViewPort animated:YES];
        
//    [self refreshAppData];
}

-(id) instanciateAndPushViewControllerWithId:(NSString *) controllerId toPageIndex:(int) pageIndex {
    UIStoryboard *storyboard;
    storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];

    UIViewController *controller = [storyboard instantiateViewControllerWithIdentifier:controllerId];
    
    controller.view.frame = CGRectMake(//
                                       self.scrollView.frame.size.width * pageIndex, //
                                       0, //
                                       self.scrollView.frame.size.width, //
                                       self.scrollView.frame.size.height);
    
    [self.scrollView addSubview:controller.view];
    [self.view layoutSubviews];
    return controller;
}

-(void) updateDateLabel {
    NSString *format;
    NSString *text;
    int currentlySelectedPeriod = self.periodSegmentControl.selectedSegmentIndex;
    NSDate *date;
    if (currentlySelectedPeriod == SRPeriodMonth) {
        NSCalendar *calendar = [NSCalendar currentCalendar];
        NSDateComponents *components = [[NSDateComponents alloc] init];
        [components setMonth:-timeOffset];
        
        date = [calendar dateByAddingComponents:components toDate:[NSDate date] options:0];
    } else if (currentlySelectedPeriod == SRPeriodWeek) {
        NSTimeInterval elapsedTimeFromNow = -timeOffset * SECOND_IN_A_WEEK;
        date = [[NSDate alloc] initWithTimeIntervalSinceNow:elapsedTimeFromNow];
    } else if (currentlySelectedPeriod == SRPeriodDay) {
        NSTimeInterval elapsedTimeFromNow = -timeOffset * SECOND_IN_A_DAY;
        date = [[NSDate alloc] initWithTimeIntervalSinceNow:elapsedTimeFromNow];
    }

    if (currentlySelectedPeriod == SRPeriodMonth) {
        format = @"MMM yyyy";
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:format];
        
        text = [formatter stringFromDate:date];
    } else if (currentlySelectedPeriod == SRPeriodWeek) {
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"dd-MM-yyyy"];
        NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
        NSDateComponents *comp = [gregorian components:NSWeekCalendarUnit fromDate:date];
        
        text =[NSString stringWithFormat:@"Semaine %d", comp.week];
    } else if (currentlySelectedPeriod == SRPeriodDay) {
        if (self.timeOffset == 0) {
            text = @"Aujourd'hui";
        } else if (self.timeOffset == 1) {
            text = @"Hier";
        } else {
            format = @"dd/MM/yyyy";
            NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
            [formatter setDateFormat:format];
            text = [formatter stringFromDate:date];
        }
    }

    [self.dateLabel setText:text];
    [self.keyNumbersViewController updateDateLabelWithDate:date andPeriod:currentlySelectedPeriod];
}

#pragma mark - UIViewController lifecycle methods
-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}

- (IBAction)periodSegmentClicked:(UISegmentedControl *)sender {
    int currentlySelectedPeriod = self.periodSegmentControl.selectedSegmentIndex;
    if (currentlySelectedPeriod == SRPeriodDay) {
        timeOffset = 1;
    } else {
        timeOffset = 0;
    }
    [self updateTimeOffsetButtonState];
    [self refreshAppData];
}

- (IBAction)screenSegmentClicked:(id)sender {
    int selectedSegmentIndex = self.screenSegmentControl.selectedSegmentIndex;
    CGFloat pageWidth = self.scrollView.contentSize.width / numScreens;
    CGFloat x = selectedSegmentIndex * pageWidth;
    [self.scrollView scrollRectToVisible:CGRectMake(x, 0, pageWidth, self.scrollView.frame.size.height) animated:YES];
}

- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView {
    pageControlUsed = NO;
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    int currentPage = lround(scrollView.contentOffset.x /
                                          (scrollView.contentSize.width / numScreens));
    self.screenSegmentControl.selectedSegmentIndex = currentPage;
}

-(void) refreshAppData {
    [SVProgressHUD show];

    [self updateDateLabel];

    int selectedSegmentIndex = self.periodSegmentControl.selectedSegmentIndex;

    NSString *period;
    if (selectedSegmentIndex == SRPeriodDay) {
        period = @"DAY";
    } else if (selectedSegmentIndex == SRPeriodWeek) {
        period = @"WEEK";
    } else if (selectedSegmentIndex == SRPeriodMonth) {
        period = @"MONTH";
    }
        
    [self fetchOrdersDataForPeriod:period];
    [self fetchPaymentsModesDataForPeriod:period];
    [self fetchShippingModesDataForPeriod:period];
    [self fetchKeyNumbersDataForPeriod:period];
    [self fetchTop10DataForPeriod:period];
    [self fetchOrderEvolutionDataForPeriod:period];
}

- (void) fetchKeyNumbersDataForPeriod:(NSString *) period {
    NSString *systemVersion = [[UIDevice currentDevice] systemVersion];
    NSString * url = [NSString stringWithFormat:@"%@/rest/v2.0/report/keyNumber?time=%@&offset=%d&server=1&client=ios_%@", SREndPoint, period, timeOffset, systemVersion];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:url] cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:10];
    
    keyNumbersRequestConnection = [[NSURLConnection alloc] init];
    (void)[keyNumbersRequestConnection initWithRequest:request delegate:self];
}

- (void) fetchOrdersDataForPeriod:(NSString *) period {
    NSString *systemVersion = [[UIDevice currentDevice] systemVersion];
    NSString * url = [NSString stringWithFormat:@"%@/rest/v2.0/report/order?time=%@&offset=%d&server=1&client=ios_%@", SREndPoint, period, timeOffset, systemVersion];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:url] cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:10];

    ordersRequestConnection = [[NSURLConnection alloc] init];
    (void)[ordersRequestConnection initWithRequest:request delegate:self];
}

- (void) fetchShippingModesDataForPeriod:(NSString *) period {
    NSString *systemVersion = [[UIDevice currentDevice] systemVersion];
    NSString * url = [NSString stringWithFormat:@"%@/rest/v2.0/report/shipping?time=%@&offset=%d&server=1&client=ios_%@", SREndPoint, period, timeOffset, systemVersion];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:url] cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:10];
    
    shippingModesRequestConnection = [[NSURLConnection alloc] init];
    (void)[shippingModesRequestConnection initWithRequest:request delegate:self];
}

- (void) fetchPaymentsModesDataForPeriod:(NSString *) period {
    NSString *systemVersion = [[UIDevice currentDevice] systemVersion];
    NSString * url = [NSString stringWithFormat:@"%@/rest/v2.0/report/payment?time=%@&offset=%d&server=1&client=ios_%@", SREndPoint, period, timeOffset, systemVersion];
    NSLog(@"URL=%@", url);
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:url] cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:10];
    
    paymentModesRequestConnection = [[NSURLConnection alloc] init];
    (void)[paymentModesRequestConnection initWithRequest:request delegate:self];
}

- (void) fetchTop10DataForPeriod:(NSString *) period {
    NSString *systemVersion = [[UIDevice currentDevice] systemVersion];
    NSString * url = [NSString stringWithFormat:@"%@/rest/v2.0/report/reference?time=%@&offset=%d&server=1&client=ios_%@", SREndPoint, period, timeOffset, systemVersion];
    NSLog(@"URL=%@", url);
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:url] cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:10];
    
    top10RequestConnection = [[NSURLConnection alloc] init];
    (void)[top10RequestConnection initWithRequest:request delegate:self];
}

- (void) fetchOrderEvolutionDataForPeriod:(NSString *) period {
    NSString *systemVersion = [[UIDevice currentDevice] systemVersion];
    NSString * url = [NSString stringWithFormat:@"%@/rest/v2.0/report/orderEvolution?time=%@&offset=%d&server=1&client=ios_%@&timeSlot=DAY", SREndPoint, period, timeOffset, systemVersion];
    NSLog(@"URL=%@", url);
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:url] cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:10];
    
    orderEvolutionRequestConnection = [[NSURLConnection alloc] init];
    (void)[orderEvolutionRequestConnection initWithRequest:request delegate:self];
}

#pragma mark NSURLConnection Delegate Methods
- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    NSHTTPURLResponse* httpResponse = (NSHTTPURLResponse*)response;
    if (connection == ordersRequestConnection) {
        ordersRequestStatus = [httpResponse statusCode];
        ordersRequestReceivedData = [[NSMutableData alloc] init];
    } else if (connection == paymentModesRequestConnection) {
        paymentModesRequestStatus = [httpResponse statusCode];
        paymentModesReceivedData = [[NSMutableData alloc] init];
    } else if (connection == shippingModesRequestConnection) {
        shippingModesRequestStatus = [httpResponse statusCode];
        shippingModesReceivedData = [[NSMutableData alloc] init];
    } else if (connection == keyNumbersRequestConnection) {
        keyNumbersRequestStatus = [httpResponse statusCode];
        keyNumbersReceivedData = [[NSMutableData alloc] init];
    } else if (connection == top10RequestConnection) {
        top10RequestStatus = [httpResponse statusCode];
        top10ReceivedData = [[NSMutableData alloc] init];
    } else if (connection == orderEvolutionRequestConnection) {
        orderEvolutionRequestStatus = [httpResponse statusCode];
        orderEvolutionReceivedData = [[NSMutableData alloc] init];
    }
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    if (connection == ordersRequestConnection) {
        [ordersRequestReceivedData appendData:data];
    } else if (connection == paymentModesRequestConnection) {
        [paymentModesReceivedData appendData:data];
    } else if (connection == shippingModesRequestConnection) {
        [shippingModesReceivedData appendData:data];
    } else if (connection == keyNumbersRequestConnection) {
        [keyNumbersReceivedData appendData:data];
    } else if (connection == top10RequestConnection) {
        [top10ReceivedData appendData:data];
    } else if (connection == orderEvolutionRequestConnection) {
        [orderEvolutionReceivedData appendData:data];
    }
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {

    if (connection == ordersRequestConnection) {
        [self parseOrdersReceivedData];
        ordersRequestConnection = nil;
    } else if (connection == paymentModesRequestConnection) {
        [self parsePaymentModesReceivedData];
        paymentModesRequestConnection = nil;
    } else if (connection == shippingModesRequestConnection) {
        [self parseShippingModesReceivedData];
        shippingModesRequestConnection = nil;
    } else if (connection == keyNumbersRequestConnection) {
        [self parseKeyNumbersReceivedData];
        keyNumbersRequestConnection = nil;
    } else if (connection == top10RequestConnection) {
        [self parseTop10ReceivedData];
        top10RequestConnection = nil;
    } else if (connection == orderEvolutionRequestConnection) {
        [self parseOrderEvolutionReceivedData];
        orderEvolutionRequestConnection = nil;
    }
    if (!ordersRequestConnection && !paymentModesRequestConnection && !keyNumbersRequestConnection && !top10RequestConnection && !orderEvolutionRequestConnection) {
        [SVProgressHUD dismiss];
    }
}

-(void) parseOrdersReceivedData {
    NSError* error;
    NSArray* json = [NSJSONSerialization
                     JSONObjectWithData:ordersRequestReceivedData
                     options:kNilOptions
                     error:&error];
    
    if (ordersRequestStatus == 204) {
        [self.webAndStoreViewController noData];
    } else if (ordersRequestStatus == 200) {
        NSMutableDictionary *values = [[NSMutableDictionary alloc] initWithCapacity:4];

        for (int i = 0; i < [json count]; i++) {
            NSDictionary* plotBar = [json objectAtIndex:i];
            NSString *type = [plotBar objectForKey:@"type"];
            NSDecimalNumber *turnOver = [plotBar objectForKey:@"turnOver"];
            NSNumber *nbOrder = [plotBar objectForKey:@"nbOrder"];
            NSString *turnOverLabel = [plotBar objectForKey:@"turnOverLabel"];
            
            if ([type isEqualToString:@"STORE"]) {
                [values setObject:turnOver forKey:SRStoreTurnOverKey];
                [values setObject:nbOrder forKey:SRStoreNumCommandsKey];
                [values setObject:turnOverLabel forKey:SRStoreTurnOverLabelKey];
            } else if ([type isEqualToString:@"WEB"]) {
                [values setObject:turnOver forKey:SRWebTurnOverKey];
                [values setObject:nbOrder forKey:SRWebNumCommandsKey];
                [values setObject:turnOverLabel forKey:SRWebTurnOverLabelKey];
            }
        }
        [self.webAndStoreViewController reloadDataWithData:values];
    } else {
        [self.webAndStoreViewController errorFetchingData];
    }
    
}

-(void) parsePaymentModesReceivedData {
    NSError* error;
    NSArray* json = [NSJSONSerialization
                     JSONObjectWithData:paymentModesReceivedData
                     options:kNilOptions
                     error:&error];
    NSNumber *amexCardNumOrders;
    NSNumber *bankCardNumOrders;
    NSNumber *checkNumOrders;
    NSNumber *giftCardNumOrders;
    NSNumber *buysterNumOrders;
    NSNumber *storeNumOrders;
    
    if (paymentModesRequestStatus == 204) {
        [self.paymentModesViewController noData];
    } else if (paymentModesRequestStatus == 200) {

        for (int i = 0; i < [json count]; i++) {
            NSDictionary* object = [json objectAtIndex:i];
            NSString *method = [object objectForKey:@"method"];
            NSNumber *nbOrders = [object objectForKey:@"nbOrder"];
        
            if ([method isEqualToString:SRAmexCardPaymentMethodKey]) {
                amexCardNumOrders = nbOrders;
            } else if ([method isEqualToString:SRBankCardPaymentMethodKey]) {
                bankCardNumOrders = nbOrders;
            } else if ([method isEqualToString:SRCheckPaymentMethodKey]) {
                checkNumOrders = nbOrders;
            } else if ([method isEqualToString:SRGiftCardPaymentMethodKey]) {
                giftCardNumOrders = nbOrders;
            } else if ([method isEqualToString:SRStorePaymentMethodKey]) {
                storeNumOrders = nbOrders;
            } else if ([method isEqualToString:SRBuysterPaymentMethodKey]) {
                buysterNumOrders = nbOrders;
            }
        }

        NSArray *values = [[NSArray alloc] initWithObjects://
                           amexCardNumOrders, //
                           bankCardNumOrders, //
                           checkNumOrders, //
                           giftCardNumOrders, //
                           storeNumOrders, //
                           buysterNumOrders, //
                           nil];
    
        [self.paymentModesViewController reloadData:values];
    } else {
        [self.paymentModesViewController errorFetchingData];
    }
}

-(void) parseShippingModesReceivedData {
    NSError* error;
    NSArray* json = [NSJSONSerialization
                     JSONObjectWithData:shippingModesReceivedData
                     options:kNilOptions
                     error:&error];
    
    NSMutableDictionary *values = [[NSMutableDictionary alloc] init];
    
    if (shippingModesRequestStatus == 204) {
        [self.shippingModesDistributionViewController noData];
    } else if (shippingModesRequestStatus == 200) {
        
        for (int i = 0; i < [json count]; i++) {
            NSDictionary* object = [json objectAtIndex:i];
            NSString *method = [object objectForKey:@"method"];
            NSNumber *nbOrders = [object objectForKey:@"nbOrder"];
            
            [values setValue:nbOrders forKey:method];
        }
        
        [self.shippingModesDistributionViewController reloadData:values];
    } else {
        [self.shippingModesDistributionViewController errorFetchingData];
    }
}

-(void) parseKeyNumbersReceivedData {
    NSError* error;
    NSDictionary* json = [NSJSONSerialization
                          JSONObjectWithData:keyNumbersReceivedData
                          options:kNilOptions
                          error:&error];

    if (keyNumbersRequestStatus == 204) {
        [self.keyNumbersViewController noData];
    } else if (keyNumbersRequestStatus == 200) {
        
        NSString *turnOver              = [json objectForKey:SRTurnOverKeyNumberKey] ;
        NSString *numProductsByOrder    = [json objectForKey:SRNumProductsByOrderKeyNumberKey];
        NSString *numOrders             = [json objectForKey:SRNumOrdersKeyNumberKey];
        NSString *numProductsSold       = [json objectForKey:SRNumProductsSoldKeyNumberKey];
        NSString *amountByOrder         = [json objectForKey:SRAmountByOrderKeyNumberKey];
    
        [self.keyNumbersViewController reloadDataWithDataWithTurnOver:turnOver //
                                            andNumProductsByOrder:numProductsByOrder //
                                                     andNumOrders:numOrders //
                                               andNumProductsSold:numProductsSold //
                                                 andAmountByOrder:amountByOrder];
    } else {
        [self.keyNumbersViewController errorFetchingData];
    }

}

-(void) parseTop10ReceivedData {
    NSError* error;
    NSArray* json = [NSJSONSerialization
                          JSONObjectWithData:top10ReceivedData
                          options:kNilOptions
                          error:&error];
    
    if (top10RequestStatus == 204) {
        [self.top10ViewController noData];
    } else if (top10RequestStatus == 200) {
        [self.top10ViewController reloadDataWithTop10:json];
    } else {
        [self.top10ViewController errorFetchingData];
    }
    
}

-(void) parseOrderEvolutionReceivedData {
    NSError* error;
    NSDictionary* json = [NSJSONSerialization
                     JSONObjectWithData:orderEvolutionReceivedData
                     options:kNilOptions
                     error:&error];
    
    if (orderEvolutionRequestStatus == 204) {
        [self.orderEvolutionViewController noData];
    } else if (orderEvolutionRequestStatus == 200) {
        NSArray *results = [json objectForKey:@"results"];
        NSArray *previousResults = [json objectForKey:@"resultsPrevious"];
        NSNumber *beginDate = [json objectForKey:@"beginDate"];
        NSNumber *endDate = [json objectForKey:@"endDate"];
        NSNumber *beginDatePrevious = [json objectForKey:@"beginDatePrevious"];
        NSNumber *endDatePrevious = [json objectForKey:@"endDatePrevious"];
        
        // Check that no value is null
        NSArray *objects = [[NSArray alloc] initWithObjects:results, previousResults, beginDate, endDate, beginDatePrevious, endDatePrevious, nil];
        if ([objects count] != 6) {
            [self.orderEvolutionViewController errorFetchingData];
            return;
        }
        
        int selectedPeriod = self.periodSegmentControl.selectedSegmentIndex;

        [self.orderEvolutionViewController //
                    reloadDataWithBeginDate:[beginDate doubleValue] / 1000 //
                    andEndDate:[endDate doubleValue] / 1000 //
                    andResults:results //
                    andBeginDatePrevious:[beginDatePrevious doubleValue] / 1000 //
                    andEndDatePrevious:[endDatePrevious doubleValue] / 1000 //
                    andPreviousResults:previousResults //
                    andSelectedPeriod : selectedPeriod];
        
    } else {
        [self.orderEvolutionViewController errorFetchingData];
    }
    
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    [SVProgressHUD dismiss];
    if (connection == ordersRequestConnection) {
        [self.webAndStoreViewController errorFetchingData];
        ordersRequestReceivedData       = nil;
        ordersRequestConnection         = nil;
    } else if (connection == paymentModesRequestConnection) {
        [self.paymentModesViewController errorFetchingData];
        paymentModesReceivedData        = nil;
        paymentModesRequestConnection   = nil;
    } else if (connection == keyNumbersRequestConnection) {
        [self.keyNumbersViewController errorFetchingData];
        keyNumbersReceivedData          = nil;
        keyNumbersRequestConnection     = nil;
    } else if (connection == top10RequestConnection) {
        [self.top10ViewController errorFetchingData];
        top10ReceivedData          = nil;
        top10RequestConnection     = nil;
    } else if (connection == orderEvolutionRequestConnection) {
        [self.orderEvolutionViewController errorFetchingData];
        orderEvolutionReceivedData          = nil;
        orderEvolutionRequestConnection     = nil;
    } else if (connection == shippingModesRequestConnection) {
        [self.shippingModesDistributionViewController errorFetchingData];
        shippingModesReceivedData          = nil;
        shippingModesRequestConnection     = nil;
    }
}

- (IBAction)onForwardClicked:(id)sender {
    if (timeOffset > 0) {
        timeOffset -= 1;
        [self refreshAppData];
    }
    [self updateTimeOffsetButtonState];
}

- (IBAction)onRewardClicked:(id)sender {
    timeOffset += 1;
    [self updateTimeOffsetButtonState];
    [self refreshAppData];
}

-(void) onTimeOffsetChanged:(int) offset {
    if (offset == 0 && offset < 0) {
        return;
    }
    timeOffset += offset;
    [self refreshAppData];
    [self updateTimeOffsetButtonState];
}

- (void) updateTimeOffsetButtonState {
    if (timeOffset == 0) {
        [forwardButton setEnabled:false];
        [self.keyNumbersViewController setForwardButtonEnabled:false];
    } else {
        [forwardButton setEnabled:true];
        [self.keyNumbersViewController setForwardButtonEnabled:true];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)viewDidUnload {
    [self setDateLabel:nil];
    [self setRewardButton:nil];
    [self setForwardButton:nil];
    [super viewDidUnload];
}
@end
