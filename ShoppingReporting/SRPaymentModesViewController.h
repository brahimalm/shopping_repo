//
//  SRPaymentModesPieChart.h
//  ShoppingReporting
//
//  Created by Boniface Mathieu on 5/28/13.
//  Copyright (c) 2013 Boniface Mathieu. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SRPaymentModesViewController : UIViewController<CPTPlotDataSource, UIActionSheetDelegate>

- (void) reloadData:(NSArray *)newValues;

-(void) noData;

-(void) errorFetchingData;

@end
