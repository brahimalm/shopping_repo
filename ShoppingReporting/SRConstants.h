//
//  SRConstants.h
//  ShoppingReporting
//
//  Created by Boniface Mathieu on 5/16/13.
//  Copyright (c) 2013 Boniface Mathieu. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SRConstants : NSObject

extern NSString * const CPDThemeNameDarkGradient;
extern NSString * const CPDThemeNamePlainBlack;
extern NSString * const CPDThemeNamePlainWhite;
extern NSString * const CPDThemeNameSlate;
extern NSString * const CPDThemeNameStocks;

extern NSString * const SRWebNumCommandsKey;
extern NSString * const SRWebTurnOverKey;
extern NSString * const SRWebTurnOverLabelKey;
extern NSString * const SRStoreNumCommandsKey;
extern NSString * const SRStoreTurnOverKey;
extern NSString * const SRStoreTurnOverLabelKey;

extern NSString * const SRAmexCardPaymentMethodKey;
extern NSString * const SRBankCardPaymentMethodKey;
extern NSString * const SRCheckPaymentMethodKey;
extern NSString * const SRGiftCardPaymentMethodKey;
extern NSString * const SRBuysterPaymentMethodKey;
extern NSString * const SRStorePaymentMethodKey;

extern NSString * const SRLADShippingModeKey;
extern NSString * const SRLDDSmallShippingModeKey;
extern NSString * const SRLDDXlShippingModeKey;
extern NSString * const SRREShippingModeKey;
extern NSString * const SRRAShippingModeKey;


extern NSString *const SRTurnOverKeyNumberKey;
extern NSString *const SRNumOrdersKeyNumberKey;
extern NSString *const SRNumProductsSoldKeyNumberKey;
extern NSString *const SRNumProductsByOrderKeyNumberKey;
extern NSString *const SRAmountByOrderKeyNumberKey;

extern int const SRPeriodDay;
extern int const SRPeriodWeek;
extern int const SRPeriodMonth;

extern NSString * const SREndPoint;

extern NSString * const SRNoDataText;
extern NSString * const SRUnableToFetchDataText;

+(UIColor *) noDataLabelColor;
+(UIColor *) unableToFetchDataLabelColor;

@end
