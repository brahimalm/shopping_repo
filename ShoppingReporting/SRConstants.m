//
//  SRConstants.m
//  ShoppingReporting
//
//  Created by Boniface Mathieu on 5/16/13.
//  Copyright (c) 2013 Boniface Mathieu. All rights reserved.
//

#import "SRConstants.h"

#ifndef SRENDPOINT

#define SRENDPOINT @"http://pfrlmaswebe01.corp.leroymerlin.com/shopping-commercial-ws-mobile"

// DEV
//@"http://dfrlmasweba03.corp.leroymerlin.com/shopping-commercial-ws-mobile";

// Recette
//@"http://rfrlmasweba03.corp.leroymerlin.com/shopping-commercial-ws-mobile";

// PPR
//@"http://afrlmaswebe01.corp.leroymerlin.com/shopping-commercial-ws-mobile";

// PRD
//@"http://pfrlmaswebe01.corp.leroymerlin.com/shopping-commercial-ws-mobile";

#endif

@implementation SRConstants

NSString * const SRWebNumCommandsKey = @"Commandes\nWeb";
NSString * const SRWebTurnOverKey = @"CA\nWeb";
NSString * const SRWebTurnOverLabelKey = @"CA\nWeb Label";

NSString * const SRStoreNumCommandsKey = @"Commandes\nmag";
NSString * const SRStoreTurnOverKey = @"CA\nMag";
NSString * const SRStoreTurnOverLabelKey = @"CA\nMag Label";

NSString * const SRAmexCardPaymentMethodKey = @"AMEX_CARD";
NSString * const SRBankCardPaymentMethodKey = @"BANK_CARD";
NSString * const SRCheckPaymentMethodKey = @"CHECK";
NSString * const SRGiftCardPaymentMethodKey = @"GIFT_CARD";
NSString * const SRBuysterPaymentMethodKey = @"BUYSTER";
NSString * const SRStorePaymentMethodKey = @"STORE";

NSString * const SRLADShippingModeKey = @"LAD";
NSString * const SRLDDSmallShippingModeKey = @"LDD_SMALL";
NSString * const SRLDDXlShippingModeKey = @"LDD_XL";
NSString * const SRREShippingModeKey = @"RE";
NSString * const SRRAShippingModeKey = @"RA";

NSString *const SRTurnOverKeyNumberKey = @"turnOver";
NSString *const SRNumOrdersKeyNumberKey = @"nbOrders";
NSString *const SRNumProductsSoldKeyNumberKey = @"nbProductsSold";
NSString *const SRNumProductsByOrderKeyNumberKey = @"nbProductsByOrder";
NSString *const SRAmountByOrderKeyNumberKey = @"amountByOrder";

int const SRPeriodDay = 0;
int const SRPeriodWeek = 1;
int const SRPeriodMonth = 2;

NSString * const SREndPoint = SRENDPOINT;

NSString * const SRNoDataText = @"Aucun résultat pour cette date";
NSString * const SRUnableToFetchDataText = @"Impossible de récupérer les données...";

+(UIColor *) noDataLabelColor {
    return [UIColor
            colorWithRed:           (float)0xFF / 0xFF // ORANGE
            green:                  (float)0x80 / 0xFF
            blue:                   (float)0x00 / 0xFF
            alpha:                  (float)0xFF / 0xFF];
}

+(UIColor *) unableToFetchDataLabelColor {
    return [UIColor
            colorWithRed:          (float)0xB0 / 0xFF // RED
            green:                 (float)0x19 / 0xFF
            blue:                  (float)0x19 / 0xFF
            alpha:                 (float)0xFF / 0xFF];
}

@end
