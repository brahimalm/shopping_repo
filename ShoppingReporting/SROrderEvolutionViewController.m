//
//  SROrderEvolutionViewController.m
//  ShoppingReporting
//
//  Created by Mathieu Boniface on 09/08/13.
//  Copyright (c) 2013 Boniface Mathieu. All rights reserved.
//

#import "SROrderEvolutionViewController.h"
#import "Foundation/NSObjCRuntime.h"
#include <math.h>

@interface SROrderEvolutionViewController ()

@property (weak, nonatomic) IBOutlet CPTGraphHostingView *hostView;
@property (weak, nonatomic) IBOutlet UILabel *noDataToDisplayLabel;

// Data helping to display results
@property (nonatomic) int selectedPeriod;
@property (strong, nonatomic) NSArray *results;
@property (strong, nonatomic) NSArray *previousResults;

@property (strong, nonatomic) CPTScatterPlot *currentPeriodPlot;
@property (strong, nonatomic) CPTScatterPlot *previousPeriodPlot;

@end

@implementation SROrderEvolutionViewController

@synthesize hostView;
@synthesize noDataToDisplayLabel;

@synthesize selectedPeriod;
@synthesize results;
@synthesize previousResults;

@synthesize currentPeriodPlot;
@synthesize previousPeriodPlot;

CPTMutableLineStyle *axisLineStyle;
CPTMutableTextStyle *axisTextStyle;

bool cumulative = NO;

NSString *  const SRPlotIdentifierPrevious      = @"PREVIOUS";
NSString *  const SRPlotIdentifierCurrent       = @"CURRENT";

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - UIViewController lifecycle methods
-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];

    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTapGesture:)];
    tapGesture.numberOfTapsRequired = 2;
    [self.view addGestureRecognizer:tapGesture];
}

- (void)handleTapGesture:(UITapGestureRecognizer *)sender {
    if (sender.state == UIGestureRecognizerStateRecognized) {
        cumulative = !cumulative;
        [self updateYAxisLabels];
        [self updateTitle];
        [hostView.hostedGraph reloadData];
    }
}

#pragma mark - Chart behavior
-(void)initPlot {
    [self configureHost];
    [self configureGraph];
    [self configurePlots];
    [self configureAxes];
    [self configureLegend];
}

-(void)configureHost {
    selectedPeriod = SRPeriodDay;
    self.hostView.allowPinchScaling = YES;
    [self.view addSubview:self.hostView];
}

-(void) configureGraph {
    // 1 - Create the graph
    CPTGraph *graph = [[CPTXYGraph alloc] initWithFrame:self.hostView.bounds];
    self.hostView.hostedGraph = graph;

    graph.paddingLeft = 0;
    graph.paddingRight = 0;
    graph.paddingTop = 20.0f;
    graph.paddingBottom = 0.0f;
    
    graph.fill = [CPTFill fillWithColor:[CPTColor clearColor]];
    graph.plotAreaFrame.fill = [CPTFill fillWithColor:[CPTColor clearColor]];
    graph.plotAreaFrame.borderLineStyle = nil;
    graph.plotAreaFrame.cornerRadius = 0;
    
    graph.plotAreaFrame.paddingLeft = 30.0f;
    graph.plotAreaFrame.paddingRight = 10.0f;
    graph.plotAreaFrame.paddingTop = 10.0f;
    graph.plotAreaFrame.paddingBottom = 50.0f;

    // Set graph title
    CPTMutableTextStyle *titleStyle = [CPTMutableTextStyle textStyle];
    titleStyle.color = [CPTColor darkGrayColor];
    titleStyle.fontName = @"HelveticaNeue-Light";
    titleStyle.fontSize = 16.0f;
    graph.titleTextStyle = titleStyle;
    graph.titlePlotAreaFrameAnchor = CPTRectAnchorTop;
    graph.titleDisplacement = CGPointMake(0.0f, 20.0f);
    
    [self updateTitle];
    
    // Enable user interactions for plot space
    CPTXYPlotSpace *plotSpace = (CPTXYPlotSpace *) graph.defaultPlotSpace;
    plotSpace.allowsUserInteraction = YES;
    plotSpace.delegate = self;
}


-(void) updateTitle {
    NSString *title = @"Répartition du nombre de commandes";
    if (cumulative) {
        title = @"Cumul du nombre de commandes";
    }
    self.hostView.hostedGraph.title = title;
}
-(void)configurePlots {
    
    // 1 - Get graph and plot space
    CPTGraph *graph = self.hostView.hostedGraph;
    CPTXYPlotSpace *plotSpace = (CPTXYPlotSpace *) graph.defaultPlotSpace;
    
    CPTColor *orange = [[CPTColor alloc]
                        initWithComponentRed:  (float)0xF5 / 0xFF // VIOLET
                        green:                 (float)0x68 / 0xFF
                        blue:                  (float)0x00 / 0xFF
                        alpha:                 (float)0xCC / 0xFF];
    CPTColor *lightOrange = [[CPTColor alloc]
                        initWithComponentRed:  (float)0xF5 / 0xFF // VIOLET
                        green:                 (float)0x68 / 0xFF
                        blue:                  (float)0x00 / 0xFF
                        alpha:                 (float)0x00 / 0xFF];

    CPTColor *green = [[CPTColor alloc]
                       initWithComponentRed:  (float)0x92 / 0xFF // VERT
                       green:                 (float)0xCE / 0xFF
                       blue:                  (float)0x14 / 0xFF
                       alpha:                 (float)0xCC / 0xFF];
    CPTColor *lightGreen = [[CPTColor alloc]
                       initWithComponentRed:  (float)0x00 / 0xFF // VERT
                       green:                 (float)0xFF / 0xFF
                       blue:                  (float)0x00 / 0xFF
                       alpha:                 (float)0x00 / 0xFF];

    previousPeriodPlot = [[CPTScatterPlot alloc] init];
    previousPeriodPlot.interpolation = CPTScatterPlotInterpolationCurved;
    previousPeriodPlot.dataSource = self;
    previousPeriodPlot.identifier = SRPlotIdentifierPrevious;
    previousPeriodPlot.title = @"";
    CPTColor *previousPeriodColor = orange;
    [graph addPlot:previousPeriodPlot toPlotSpace:plotSpace];
    
    currentPeriodPlot = [[CPTScatterPlot alloc] init];
    currentPeriodPlot.interpolation = CPTScatterPlotInterpolationCurved;
    currentPeriodPlot.dataSource = self;
    currentPeriodPlot.identifier = SRPlotIdentifierCurrent;
    currentPeriodPlot.title = @"";
    CPTColor *currentPeriodColor = green;
    [graph addPlot:currentPeriodPlot toPlotSpace:plotSpace];

    [plotSpace scaleToFitPlots:[NSArray arrayWithObjects:currentPeriodPlot, previousPeriodPlot, nil]];
    
    CPTMutableLineStyle *currentPeriodLineStyle = [currentPeriodPlot.dataLineStyle mutableCopy];
    currentPeriodLineStyle.lineWidth = 1.5;
    currentPeriodLineStyle.lineColor = currentPeriodColor;
    currentPeriodPlot.dataLineStyle = currentPeriodLineStyle;
    
    CPTGradient *currentAreaGradient = [CPTGradient gradientWithBeginningColor:green endingColor:lightGreen];
    currentAreaGradient.angle = -90.0;
    currentPeriodPlot.areaFill = [CPTFill fillWithGradient:currentAreaGradient];
    currentPeriodPlot.areaBaseValue = CPTDecimalFromInteger(0);

    CPTGradient *previousAreaGradient = [CPTGradient gradientWithBeginningColor:orange endingColor:lightOrange];
    previousAreaGradient.angle = -90.0;
    previousPeriodPlot.areaFill = [CPTFill fillWithGradient:previousAreaGradient];
    previousPeriodPlot.areaBaseValue = CPTDecimalFromInteger(0);
    
    CPTMutableLineStyle *previousPeriodLineStyle = [previousPeriodPlot.dataLineStyle mutableCopy];
    previousPeriodLineStyle.lineWidth = 1.5;
    previousPeriodLineStyle.lineColor = previousPeriodColor;
    previousPeriodPlot.dataLineStyle = previousPeriodLineStyle;

    CPTPlotSymbol *currentPeriodPlotSymbol = [CPTPlotSymbol ellipsePlotSymbol];
    currentPeriodPlotSymbol.fill = [CPTFill fillWithColor:currentPeriodColor];
    currentPeriodPlotSymbol.lineStyle = currentPeriodLineStyle;
    currentPeriodPlotSymbol.size = CGSizeMake(2.0f, 2.0f);
    currentPeriodPlot.plotSymbol = currentPeriodPlotSymbol;

    CPTPlotSymbol *previousPeriodPlotSymbol = [CPTPlotSymbol ellipsePlotSymbol];
    previousPeriodPlotSymbol.fill = [CPTFill fillWithColor:previousPeriodColor];
    previousPeriodPlotSymbol.lineStyle = previousPeriodLineStyle;
    previousPeriodPlotSymbol.size = CGSizeMake(2.0f, 2.0f);
    previousPeriodPlot.plotSymbol = previousPeriodPlotSymbol;

}

-(void)configureAxes {

    CPTColor *axisLineColor = [CPTColor darkGrayColor];
    
    axisLineStyle = [CPTMutableLineStyle lineStyle];
    axisLineStyle.lineWidth = 1.5f;
    axisLineStyle.lineColor = axisLineColor;
    axisTextStyle = [[CPTMutableTextStyle alloc] init];
    axisTextStyle.color = [CPTColor darkGrayColor];
    axisTextStyle.fontName = @"Helvetica-Bold";
    axisTextStyle.fontSize = 11.0f;
    CPTMutableLineStyle *tickLineStyle = [CPTMutableLineStyle lineStyle];
    tickLineStyle.lineColor = [CPTColor darkGrayColor];
    tickLineStyle.lineWidth = 1.5f;
    
    CPTXYAxisSet *axisSet = (CPTXYAxisSet *) self.hostView.hostedGraph.axisSet;
    
    // Configure x axis
    CPTXYAxis *x = axisSet.xAxis;
    
    x.axisConstraints = [CPTConstraints constraintWithLowerOffset:0.0];
    x.axisLineStyle = axisLineStyle;
    
    x.labelingPolicy = CPTAxisLabelingPolicyNone;
    x.labelTextStyle = axisTextStyle;
    x.labelOffset = 10.0f;
    
    x.tickDirection = CPTSignPositive;
    
    x.minorTickLineStyle = axisLineStyle;
    x.minorTickLength = 4.0f;
    
    x.majorTickLineStyle = axisLineStyle;
    x.majorTickLength = 6.0f;
        
    // Configure y axis    
    CPTXYAxis *y = (CPTXYAxis *) axisSet.yAxis;

    y.axisConstraints = [CPTConstraints constraintWithLowerOffset:0.0];
    y.axisLineStyle = axisLineStyle;
    y.labelingPolicy = CPTAxisLabelingPolicyNone;
    y.labelTextStyle = axisTextStyle;
    y.labelOffset = 15.0f;
    
    y.tickDirection = CPTSignPositive;
    
    y.minorTickLineStyle = axisLineStyle;
    y.minorTickLength = 4.0f;
    
    y.majorTickLineStyle = axisLineStyle;
    y.majorTickLength = 6.0f;
    
    CPTMutableLineStyle *gridLineStyle = [CPTMutableLineStyle lineStyle];
    gridLineStyle.lineWidth = 0.3f;
    gridLineStyle.lineColor = axisLineColor;
    y.majorGridLineStyle = gridLineStyle;
}

-(void) updateAxisLabels {
    if (selectedPeriod == SRPeriodDay) {
        [self updateXAxisLabelsForDay];
        [self updateYAxisLabels];
    } else if (selectedPeriod == SRPeriodWeek) {
        [self updateXAxisLabelsForWeek];
        [self updateYAxisLabels];
    } else if (selectedPeriod == SRPeriodMonth) {
        [self updateXAxisLabelsForMonth];
        [self updateYAxisLabels];
    }
}

-(void) updateYAxisLabels {
    
    CPTXYAxisSet *axisSet = (CPTXYAxisSet *) self.hostView.hostedGraph.axisSet;

    CPTXYAxis *y = (CPTXYAxis *) axisSet.yAxis;

    int yMax = [self maxValue];
    yMax = yMax + yMax * 0.1f;
    yMax = MAX(yMax, 10);
    
    int numTicks = MIN(yMax, 30);
    NSMutableSet *yLabels  = [NSMutableSet setWithCapacity:numTicks];
    NSMutableSet *yMajorLocations = [NSMutableSet setWithCapacity:numTicks / 2];
    NSMutableSet *yMinorLocations = [NSMutableSet setWithCapacity:numTicks / 2];
    
    for (int i = 0 ; i < numTicks * 2  ; i++) {
        NSString *tickLabel;
        BOOL isMajorTick = (i+1) % 2 == 1;
        
        if (isMajorTick || yMax < 30) {
            tickLabel = [NSString stringWithFormat:@"%d", (i) * (yMax / numTicks)];
            [yMajorLocations addObject:[NSNumber numberWithFloat:(i) * (yMax / numTicks)]];
        } else {
            tickLabel = @"";
            [yMinorLocations addObject:[NSNumber numberWithFloat:(i) * (yMax / numTicks)]];
        }
        
        CPTAxisLabel *label = [[CPTAxisLabel alloc] initWithText:tickLabel  textStyle:axisTextStyle];
        
        label.tickLocation = CPTDecimalFromCGFloat((i) * (yMax / numTicks));
        label.offset = - (log10(yMax) * 5 + 8);
        
        [yLabels addObject:label];
    }
    
    y.axisLabels = yLabels;
    y.minorTickLocations = yMinorLocations;
    y.majorTickLocations = yMajorLocations;
    y.visibleRange = [CPTPlotRange plotRangeWithLocation:CPTDecimalFromFloat(0)
                                                  length:CPTDecimalFromFloat(yMax)];
    
    CPTXYPlotSpace *plotSpace = (CPTXYPlotSpace *) self.hostView.hostedGraph.defaultPlotSpace;
    
    plotSpace.globalYRange = [CPTPlotRange plotRangeWithLocation:CPTDecimalFromFloat(0)
                                                          length:CPTDecimalFromFloat(yMax)];
    
    // Initialize the view port position
    float yMin = [self minValue];
    yMin = yMin - (yMin * 0.10f);
    plotSpace.yRange = [CPTPlotRange plotRangeWithLocation:CPTDecimalFromFloat(yMin)
                                                    length:CPTDecimalFromFloat(yMax - yMin)];
}

-(void) updateXAxisLabelsForDay {
    CPTXYAxisSet *axisSet = (CPTXYAxisSet *) self.hostView.hostedGraph.axisSet;
    CPTXYAxis *x = axisSet.xAxis;
    
    NSMutableSet *xLabels  = [NSMutableSet setWithCapacity:24];
    NSMutableSet *xMajorLocations = [NSMutableSet setWithCapacity:12];
    NSMutableSet *xMinorLocations = [NSMutableSet setWithCapacity:12];
    
    for (int i = 0 ; i < 24  ; i++) {
        NSString *tickLabel;
        BOOL isOddDay = (i+1) % 2 == 1;
        
        if (isOddDay) {
            tickLabel = [NSString stringWithFormat:@"%dh", i];;
            [xMajorLocations addObject:[NSNumber numberWithFloat:i]];
        } else {
            tickLabel = @"";
            [xMinorLocations addObject:[NSNumber numberWithFloat:i]];
        }
        
        CPTAxisLabel *label = [[CPTAxisLabel alloc] initWithText:tickLabel  textStyle:x.labelTextStyle];
        
        label.tickLocation = CPTDecimalFromCGFloat(i);
        label.offset = - x.majorTickLength - x.labelOffset;
        
        [xLabels addObject:label];
    }
    
    x.axisLabels = xLabels;
    x.minorTickLocations = xMinorLocations;
    x.majorTickLocations = xMajorLocations;
    x.visibleRange = [CPTPlotRange plotRangeWithLocation:CPTDecimalFromFloat(0)
                                                  length:CPTDecimalFromFloat(23)];
    
    CPTXYPlotSpace *plotSpace = (CPTXYPlotSpace *) self.hostView.hostedGraph.defaultPlotSpace;
    
    plotSpace.globalXRange = [CPTPlotRange plotRangeWithLocation:CPTDecimalFromFloat(0)
                                                          length:CPTDecimalFromFloat(23)];
    plotSpace.xRange = [CPTPlotRange plotRangeWithLocation:CPTDecimalFromFloat(0)
                                                    length:CPTDecimalFromFloat(23)];
}

-(void) updateXAxisLabelsForWeek {
    CPTXYAxisSet *axisSet = (CPTXYAxisSet *) self.hostView.hostedGraph.axisSet;
    CPTXYAxis *x = axisSet.xAxis;
    
    NSMutableSet *xLabels  = [NSMutableSet setWithCapacity:7];
    NSMutableSet *xMajorLocations = [NSMutableSet setWithCapacity:7];
    NSMutableSet *xMinorLocations = [NSMutableSet setWithCapacity:7];
    
    NSArray *days = [[NSArray alloc] initWithObjects:@"Lun", @"Mar", @"Mer", @"Jeu", @"Ven", @"Sam", @"Dim", nil];
    
    for (int i = 0 ; i < 7  ; i++) {
        NSString *tickLabel;
        BOOL isOddDay = (i+1) % 2 == 1;
        
        if (isOddDay) {
            tickLabel = [days objectAtIndex:i];
            [xMajorLocations addObject:[NSNumber numberWithFloat:i]];
        } else {
            tickLabel = [days objectAtIndex:i];
            [xMinorLocations addObject:[NSNumber numberWithFloat:i]];
        }
        
        CPTAxisLabel *label = [[CPTAxisLabel alloc] initWithText:tickLabel  textStyle:x.labelTextStyle];
        
        label.tickLocation = CPTDecimalFromCGFloat(i);
        label.offset = - x.majorTickLength - x.labelOffset;
        
        [xLabels addObject:label];
    }
    
    x.axisLabels = xLabels;
    x.minorTickLocations = xMinorLocations;
    x.majorTickLocations = xMajorLocations;
    x.visibleRange = [CPTPlotRange plotRangeWithLocation:CPTDecimalFromFloat(0)
                                                  length:CPTDecimalFromFloat(6)];
    
    CPTXYPlotSpace *plotSpace = (CPTXYPlotSpace *) self.hostView.hostedGraph.defaultPlotSpace;
    
    plotSpace.globalXRange = [CPTPlotRange plotRangeWithLocation:CPTDecimalFromFloat(0)
                                                          length:CPTDecimalFromFloat(6)];
    plotSpace.xRange = [CPTPlotRange plotRangeWithLocation:CPTDecimalFromFloat(0)
                                                    length:CPTDecimalFromFloat(6)];
}


-(void) updateXAxisLabelsForMonth {
    CPTXYAxisSet *axisSet = (CPTXYAxisSet *) self.hostView.hostedGraph.axisSet;
    CPTXYAxis *x = axisSet.xAxis;
    
    int numDaysInMonth = MAX([results count], [previousResults count]);
    
    int numOddDaysInMonth  = numDaysInMonth / 2 + numDaysInMonth % 2;
    int numEvenDaysInMonth = numDaysInMonth / 2;
    
    NSMutableSet *xLabels  = [NSMutableSet setWithCapacity:numDaysInMonth];
    NSMutableSet *xMajorLocations = [NSMutableSet setWithCapacity:numOddDaysInMonth];
    NSMutableSet *xMinorLocations = [NSMutableSet setWithCapacity:numEvenDaysInMonth];
    
    for (int i = 0 ; i < numDaysInMonth ; i++) {
        NSString *tickLabel;
        BOOL isOddDay = (i+1) % 2 == 1;
        
        if (isOddDay) {
            tickLabel = [NSString stringWithFormat:@"%d", i+1];
            [xMajorLocations addObject:[NSNumber numberWithFloat:i]];
        } else {
            tickLabel = @"";
            [xMinorLocations addObject:[NSNumber numberWithFloat:i]];
        }
        
        CPTAxisLabel *label = [[CPTAxisLabel alloc] initWithText:tickLabel  textStyle:x.labelTextStyle];
        
        label.tickLocation = CPTDecimalFromCGFloat(i);
        label.offset = - x.majorTickLength - x.labelOffset;
        
        [xLabels addObject:label];
    }
    
    x.axisLabels = xLabels;
    x.minorTickLocations = xMinorLocations;
    x.majorTickLocations = xMajorLocations;
    x.visibleRange = [CPTPlotRange plotRangeWithLocation:CPTDecimalFromFloat(0)
                                                  length:CPTDecimalFromFloat(numDaysInMonth - 1)];
   
    CPTXYPlotSpace *plotSpace = (CPTXYPlotSpace *) self.hostView.hostedGraph.defaultPlotSpace;
    
    plotSpace.globalXRange = [CPTPlotRange plotRangeWithLocation:CPTDecimalFromFloat(0)
                                                          length:CPTDecimalFromFloat(numDaysInMonth - 1)];
    plotSpace.xRange = [CPTPlotRange plotRangeWithLocation:CPTDecimalFromFloat(0)
                                                   length:CPTDecimalFromFloat(numDaysInMonth - 1)];
}

-(int) maxValue {
    if (cumulative) {
        int maxValue1 = 0;
        int maxValue2 = 0;
        for (NSNumber *value in results) {
            maxValue1 = maxValue1 + [value intValue];
        }
        for (NSNumber *value in previousResults) {
            maxValue2 = maxValue2 + [value intValue];
        }
        return maxValue1 > maxValue2 ?  maxValue1 : maxValue2;
    } else {
        int maxValue = 0;
        for (NSNumber *value in results) {
            if (maxValue  < [value intValue]) {
                maxValue = [value intValue];
            }
        }
        for (NSNumber *value in previousResults) {
            if (maxValue  < [value intValue]) {
                maxValue = [value intValue];
            }
        }
        return maxValue;
    }
}

- (CPTFill *)barFillForBarPlot:(CPTBarPlot *)barPlot recordIndex:(NSUInteger)index {
    return [CPTFill fillWithGradient:[CPTGradient gradientWithBeginningColor:[CPTColor redColor] endingColor:[CPTColor blackColor]]];
}

- (int) getDayInMonthWithDate:(NSDate *) date {
    NSCalendar* calendar = [NSCalendar currentCalendar];
    NSDateComponents* components = [calendar components:NSDayCalendarUnit fromDate:date];
    
    int dayInMonth = [components day];
    
    return dayInMonth;
}

-(int) minValue {
    int minValue = 100000;
    for (NSNumber *value in results) {
        if (minValue > [value intValue]) {
            minValue = [value intValue];
        }
    }
    for (NSNumber *value in previousResults) {
        if (minValue > [value intValue]) {
            minValue = [value intValue];
        }
    }
    return minValue;
}

#pragma mark - CPTPlotDataSource methods
-(NSUInteger)numberOfRecordsForPlot:(CPTPlot *)plot {
    if (plot == currentPeriodPlot) {
        return [results count];
    }
    if (plot == previousPeriodPlot) {
        return [previousResults count];
    }
    return 0;
}

-(CPTPlotRange *)plotSpace:(CPTPlotSpace *)space willChangePlotRangeTo:(CPTPlotRange *)newRange forCoordinate:(CPTCoordinate)coordinate{
    if (coordinate == CPTCoordinateX) {
        newRange = ((CPTXYPlotSpace*)space).xRange;
    }
    return newRange;
}

-(NSNumber *)numberForPlot:(CPTPlot *)plot field:(NSUInteger)fieldEnum recordIndex:(NSUInteger)index {
    switch (fieldEnum) {
        case CPTScatterPlotFieldX:
            return [NSNumber numberWithUnsignedInteger:index];
        case CPTScatterPlotFieldY:
            if ([plot.identifier isEqual:SRPlotIdentifierCurrent] == YES) {
                if (index >= [results count]) {
                    return [NSNumber numberWithFloat:0];
                }
                if (cumulative) {
                    int value = 0;
                    for (int i = 0 ; i <= index ; i++) {
                        value = value + [[results objectAtIndex:i] integerValue];
                    }
                    return [NSNumber numberWithInt:value];
                } else {
                    return [results objectAtIndex:index];
                }
            } else if ([plot.identifier isEqual:SRPlotIdentifierPrevious] == YES) {
                if (index >= [previousResults count]) {
                    return [NSNumber numberWithFloat:0];
                }
                if (cumulative) {
                    int value = 0;
                    for (int i = 0 ; i <= index ; i++) {
                        value += [[previousResults objectAtIndex:i] integerValue];
                    }
                    return [NSNumber numberWithInt:value];
                } else {
                    return [previousResults objectAtIndex:index];
                }
            }
            break;
    }
    return [NSDecimalNumber zero];
}

-(void) reloadDataWithBeginDate: (NSTimeInterval) newBeginDate
                     andEndDate: (NSTimeInterval) newEndDate
                     andResults: (NSArray *)      newResults
           andBeginDatePrevious: (NSTimeInterval) newBeginDatePrevious
             andEndDatePrevious: (NSTimeInterval) newEndDatePrevious
             andPreviousResults: (NSArray *)      newPreviousResults
              andSelectedPeriod: (int)            newSelectedPeriod {
    
    if (self.hostView.hostedGraph == nil) {
        [self initPlot];
    }

    self.selectedPeriod = newSelectedPeriod;

    NSDate *beginDate = [NSDate dateWithTimeIntervalSince1970:newBeginDate];
    NSDate *beginDatePrevious = [NSDate dateWithTimeIntervalSince1970:newBeginDatePrevious];
    
    results = newResults;
    previousResults = newPreviousResults;

    [self updateLegendLabelsWithBeginDatePrevious: beginDatePrevious andBeginDateCurrent:beginDate];
    [self updateAxisLabels];
    [hostView.hostedGraph reloadData];
    
    [hostView setHidden:NO];
    [noDataToDisplayLabel setHidden:YES];
    
//    int i = 0;
//    for (NSNumber *value in results) {
//        NSLog(@"results\t\t%d:%d", i++, [value intValue]);
//    }
//    i = 0;
//    for (NSNumber *value in previousResults) {
//        NSLog(@"previousResults\t%d:%d", i++, [value intValue]);
//    }
    
}

-(void) updateLegendLabelsWithBeginDatePrevious:(NSDate *) beginDatePrevious andBeginDateCurrent:(NSDate *) beginDateCurrent {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    
    if (selectedPeriod == SRPeriodDay) {
        [dateFormatter setDateFormat:@"dd/MM/yyyy"];
    } else if (selectedPeriod == SRPeriodWeek) {
        [dateFormatter setDateFormat:@"'Semaine' ww"];
    } else if (selectedPeriod == SRPeriodMonth) {
        [dateFormatter setDateFormat:@"MMM yyyy"];
    }
   
    NSString *beginDatePreviousString = [dateFormatter stringFromDate:beginDatePrevious];
    previousPeriodPlot.title = beginDatePreviousString;
    
    NSString *beginDateCurrentString = [dateFormatter stringFromDate:beginDateCurrent];
    currentPeriodPlot.title = beginDateCurrentString;
}

-(void) noData {
    [hostView setHidden:YES];
    [noDataToDisplayLabel setTextColor:[SRConstants noDataLabelColor]];
    [noDataToDisplayLabel setText:SRNoDataText];
    [noDataToDisplayLabel setHidden:NO];
}

-(void) errorFetchingData {
    [hostView setHidden:YES];
    [noDataToDisplayLabel setTextColor:[SRConstants unableToFetchDataLabelColor]];
    [noDataToDisplayLabel setText:SRUnableToFetchDataText];
    [noDataToDisplayLabel setHidden:NO];
}

-(void) configureLegend {
    CPTGraph *graph = self.hostView.hostedGraph;
    CPTLegend *theLegend = [CPTLegend legendWithGraph:graph];
    
    theLegend.numberOfColumns = 2;
    
    CPTMutableTextStyle *legendTextStyle = [[CPTMutableTextStyle alloc] init];
    legendTextStyle.fontSize = 14.0f;
    [legendTextStyle setColor:[CPTColor darkGrayColor]];
    
    theLegend.textStyle = legendTextStyle;
    
    theLegend.fill = [CPTFill fillWithColor:[CPTColor whiteColor]];
    theLegend.cornerRadius = 5.0;
    
    graph.legend = theLegend;
    graph.legendAnchor = CPTRectAnchorBottom;
    graph.legendDisplacement = CGPointMake(0, 5);    
}

- (void)viewDidUnload {
    [self setHostView:nil];
    [self setNoDataToDisplayLabel:nil];
    [super viewDidUnload];
}

@end
