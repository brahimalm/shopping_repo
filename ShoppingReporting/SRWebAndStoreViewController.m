//
//  SPFirstViewController.m
//  ShoppingReporting
//
//  Created by Boniface Mathieu on 5/3/13.
//  Copyright (c) 2013 Boniface Mathieu. All rights reserved.
//

#import "SRWebAndStoreViewController.h"
#import "CPTFill.h"

#define DegreesToRadians(x) (M_PI * x / 180.0)

@interface SRWebAndStoreViewController ()

@property (nonatomic, strong) IBOutlet CPTGraphHostingView *hostView;
@property (nonatomic, strong) CPTPieChart *turnOverPieChart;
@property (nonatomic, strong) CPTPieChart *numCommandsPieChart;
@property (nonatomic, strong) NSDictionary *values;
@property (weak, nonatomic) IBOutlet UILabel *noDataToDisplayLabel;

-(void)configureGraph;
-(void)configureChart;

@end

@implementation SRWebAndStoreViewController

@synthesize hostView;
@synthesize turnOverPieChart;
@synthesize numCommandsPieChart;
@synthesize values;
@synthesize noDataToDisplayLabel;

NSString * const SRNumCommandsPieChartIdentifier = @"numcommands";
NSString * const SRTurnOverPieChartIdentifier = @"turnover";


#pragma mark - CPTPlotDataSource methods
-(NSUInteger)numberOfRecordsForPlot:(CPTPlot *)plot {
    return 2;
}

-(void) viewDidLoad {
    [self initPlot];
}

-(CPTFill *)sliceFillForPieChart:(CPTPieChart *)pieChart recordIndex:(NSUInteger)index {
    NSArray * colors;
    if ([pieChart.identifier isEqual:SRNumCommandsPieChartIdentifier]) {
        CPTColor *webNumCommandsPlotColor = [[CPTColor alloc]
                                             initWithComponentRed:  (float)0x92 / 0xFF // VERT
                                             green:                 (float)0xCE / 0xFF
                                             blue:                  (float)0x14 / 0xFF
                                             alpha:                 (float)0xFF / 0xFF];
        CPTColor *storeNumCommandsPlotColor = [[CPTColor alloc]
                                                  initWithComponentRed:  (float)0xB7 / 0xFF // ROUGE
                                                  green:                 (float)0x22 / 0xFF
                                                  blue:                  (float)0x1C / 0xFF
                                                  alpha:                 (float)0xFF / 0xFF];
        colors = [[NSArray alloc] initWithObjects:storeNumCommandsPlotColor, webNumCommandsPlotColor, nil];
    } else {
        CPTColor *webTurnOverPlotColor = [[CPTColor alloc]
                                          initWithComponentRed:  (float)0xEE / 0xFF // ORANGE
                                          green:                 (float)0x87 / 0xFF
                                          blue:                  (float)0x06 / 0xFF
                                          alpha:                 (float)0xFF / 0xFF];
        CPTColor *storeTurnOverPlotColor = [[CPTColor alloc]
                                               initWithComponentRed:  (float)0x6B / 0xFF // VIOLET
                                               green:                 (float)0x21 / 0xFF
                                               blue:                  (float)0x9E / 0xFF
                                               alpha:                 (float)0xFF / 0xFF];
        colors = [[NSArray alloc] initWithObjects:storeTurnOverPlotColor, webTurnOverPlotColor, nil];
        
    }

    return [CPTFill fillWithColor:[colors objectAtIndex:index]];
}


-(NSNumber *)numberForPlot:(CPTPlot *)plot field:(NSUInteger)fieldEnum recordIndex:(NSUInteger)index {
    NSNumber *value;
    
    if ([plot.identifier isEqual:SRTurnOverPieChartIdentifier]) {
        if (index == 0) {
            value = [values objectForKey:SRStoreTurnOverKey];
        } else {
            value = [values objectForKey:SRWebTurnOverKey];
        }
    } else {
        if (index == 0) {
            value = [values objectForKey:SRStoreNumCommandsKey];
        } else {
            value = [values objectForKey:SRWebNumCommandsKey];
        }
    }
    return value;
}

-(CPTLayer *)dataLabelForPlot:(CPTPlot *)plot recordIndex:(NSUInteger)index {
    
    static CPTMutableTextStyle *labelText = nil;
    double value;
    double valuesSum;
    
    if (!labelText) {
        labelText= [[CPTMutableTextStyle alloc] init];
        labelText.color = [CPTColor darkGrayColor];
    }
    
    NSString *labelValue = nil;
    double percent = 0;

    
    if ([plot.identifier isEqual:SRTurnOverPieChartIdentifier]) {
        if (index == 0) {
            value = [[values objectForKey:SRStoreTurnOverKey] doubleValue];
            labelValue = [values objectForKey:SRStoreTurnOverLabelKey];
        } else {
            value = [[values objectForKey:SRWebTurnOverKey] doubleValue];
            labelValue = [values objectForKey:SRWebTurnOverLabelKey];
        }
        
        valuesSum = [[values objectForKey:SRStoreTurnOverKey] doubleValue] //
                + [[values objectForKey:SRWebTurnOverKey] doubleValue];
        
        percent = value / valuesSum;
        
        if (index == 0) {
            double angle = (percent) * 2 * M_PI;
            double startAngle = 5 * M_PI_4 / 4 + angle / 2;
            turnOverPieChart.startAngle = startAngle;
        }
        
        if (value > 10000) {
            labelValue = [NSString stringWithFormat:@"%@ (%0.1f %%)", labelValue, percent * 100.0f];
        } else {
            labelValue = [NSString stringWithFormat:@"%@ (%0.1f %%)", labelValue, percent * 100.0f];
        }

    } else {
        if (index == 0) {
            value = [[values objectForKey:SRStoreNumCommandsKey] doubleValue];
        } else {
            value = [[values objectForKey:SRWebNumCommandsKey] doubleValue];
        }
        valuesSum = [[values objectForKey:SRStoreNumCommandsKey] doubleValue] + [[values objectForKey:SRWebNumCommandsKey] doubleValue];
        percent = value / valuesSum;

        if (index == 0) {
            double angle = (percent) * 2 * M_PI;
            double startAngle = 5 * M_PI_4 / 4 + angle / 2;
            numCommandsPieChart.startAngle = startAngle;
        }

        labelValue = [NSString stringWithFormat:@"%0.0f (%0.1f %%)", value, percent * 100.0f];
    }
    CPTTextLayer *labelLayer = [[CPTTextLayer alloc] initWithText:labelValue style:labelText];
    return labelLayer;
}

-(NSString *)legendTitleForPieChart:(CPTPieChart *)pieChart recordIndex:(NSUInteger)index {
    if (index == 0 && [pieChart.identifier isEqual:SRTurnOverPieChartIdentifier]) {
        return @"Demande retrait mag";
    } else if (index == 1 && [pieChart.identifier isEqual:SRTurnOverPieChartIdentifier]) {
        return @"Demande cmds web";
    } else if (index == 0 && [pieChart.identifier isEqual:SRNumCommandsPieChartIdentifier]) {
        return @"Nbre cmds retrait mag";
    } else if (index == 1 && [pieChart.identifier isEqual:SRNumCommandsPieChartIdentifier]) {
        return @"Nbre cmds web";
    }
    return @"N/A";
}

#pragma mark - Rotation
-(BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    return (interfaceOrientation == UIInterfaceOrientationLandscapeLeft);
}

#pragma mark - Chart behavior
-(void)initPlot {
    [self configureGraph];
    [self configureChart];
    [self configureLegend];
}

-(void) configureLegend {
    CPTGraph *graph = self.hostView.hostedGraph;
    CPTLegend *theLegend = [CPTLegend legendWithGraph:graph];
    
    theLegend.numberOfColumns = 2;
    
    CPTMutableTextStyle *legendTextStyle = [[CPTMutableTextStyle alloc] init];
    legendTextStyle.fontSize = 12.0f;
    [legendTextStyle setColor:[CPTColor whiteColor]];
    
    theLegend.textStyle = legendTextStyle;
    
    theLegend.fill = [CPTFill fillWithColor:[CPTColor grayColor]];
    theLegend.cornerRadius = 5.0;
    
    graph.legend = theLegend;
    graph.legendAnchor = CPTRectAnchorBottom;
    graph.legendDisplacement = CGPointMake(0, 5);

}

-(void)configureGraph {
    
    CPTGraph *graph = [[CPTXYGraph alloc] init];
    self.hostView.hostedGraph = graph;

    [graph applyTheme:[CPTTheme themeNamed:kCPTDarkGradientTheme]];
    graph.plotAreaFrame.cornerRadius = 0;

    graph.fill = [CPTFill fillWithColor:[CPTColor clearColor]];
    graph.plotAreaFrame.fill = [CPTFill fillWithColor:[CPTColor clearColor]];

    graph.paddingBottom = 3.0f;
    graph.paddingLeft  = 3.0f;
    graph.paddingTop    = 0.0f;
    graph.paddingRight  = 3.0f;
    graph.axisSet = nil;
    
    CPTMutableTextStyle *titleStyle = [CPTMutableTextStyle textStyle];
    titleStyle.color = [CPTColor darkGrayColor];
    titleStyle.fontName = @"HelveticaNeue-Light";
    titleStyle.fontSize = 16.0f;
    titleStyle.textAlignment = CPTTextAlignmentCenter;

    graph.title = @"Répartition de la demande en € \net des commandes";
    graph.titleTextStyle = titleStyle;
    graph.titlePlotAreaFrameAnchor = CPTRectAnchorTop;
    graph.titleDisplacement = CGPointMake(0, -3.0f);

    graph.plotAreaFrame.borderLineStyle = nil;
    graph.plotAreaFrame.paddingBottom = 5.0f;
    graph.plotAreaFrame.paddingLeft  = 5.0f;
    graph.plotAreaFrame.paddingTop    = 0.0f;
    graph.plotAreaFrame.paddingRight  = 5.0f;
}

-(void)configureChart {

    CPTGraph *graph = self.hostView.hostedGraph;
    turnOverPieChart = [[CPTPieChart alloc] init];
    turnOverPieChart.dataSource = self;
    turnOverPieChart.delegate = self;
    if (SYSTEM_SCREEN_HEIGHT_GREATER_THAN(480)) {
        turnOverPieChart.pieRadius = 60;
    } else {
        turnOverPieChart.pieRadius = 50;
    }
    turnOverPieChart.identifier = SRTurnOverPieChartIdentifier;
    turnOverPieChart.sliceDirection = CPTPieDirectionClockwise;
    turnOverPieChart.centerAnchor = CGPointMake(0.5, 0.70);

    numCommandsPieChart = [[CPTPieChart alloc] init];
    numCommandsPieChart.dataSource = self;
    numCommandsPieChart.delegate = self;
    if (SYSTEM_SCREEN_HEIGHT_GREATER_THAN(480)) {
        numCommandsPieChart.pieRadius = 60;
    } else {
        numCommandsPieChart.pieRadius = 50;
    }
    numCommandsPieChart.identifier = SRNumCommandsPieChartIdentifier;
    numCommandsPieChart.sliceDirection = CPTPieDirectionClockwise;
    if (SYSTEM_SCREEN_HEIGHT_GREATER_THAN(480)) {
        numCommandsPieChart.centerAnchor = CGPointMake(0.5, 0.33);
    } else {
        numCommandsPieChart.centerAnchor = CGPointMake(0.5, 0.35);
    }

    [graph addPlot:turnOverPieChart];
    
    CPTGradient *overlayGradient = [[CPTGradient alloc] init];
    overlayGradient.gradientType = CPTGradientTypeRadial;
    
    CPTColor *numCommandsGradientColor = [CPTColor darkGrayColor];
    
    overlayGradient = [overlayGradient addColorStop:[numCommandsGradientColor colorWithAlphaComponent:0.0] atPosition:0.5];
    overlayGradient = [overlayGradient addColorStop:[numCommandsGradientColor colorWithAlphaComponent:0.4] atPosition:1.0];
    
    numCommandsPieChart.overlayFill = [CPTFill fillWithGradient:overlayGradient];
    turnOverPieChart.overlayFill =  [CPTFill fillWithGradient:overlayGradient];
    [graph addPlot:numCommandsPieChart];

}

- (void) reloadDataWithData:(NSDictionary *)newValues {
    values = newValues;
    
    if (newValues != nil && [newValues count] >= 4) {
        [hostView setHidden:NO];
        [noDataToDisplayLabel setHidden:YES];
        [turnOverPieChart reloadData];
        [numCommandsPieChart reloadData];
        [self configureLegend];
    } else {
        [self errorFetchingData];
    }
}

-(void) noData {
    [hostView setHidden:YES];
    [noDataToDisplayLabel setTextColor:[SRConstants noDataLabelColor]];
    [noDataToDisplayLabel setText:SRNoDataText];
    [noDataToDisplayLabel setHidden:NO];
}

-(void) errorFetchingData {
    [hostView setHidden:YES];
    [noDataToDisplayLabel setTextColor:[SRConstants unableToFetchDataLabelColor]];
    [noDataToDisplayLabel setText:SRUnableToFetchDataText];
    [noDataToDisplayLabel setHidden:NO];
}

- (void)viewDidUnload {
    [self setNoDataToDisplayLabel:nil];
    [super viewDidUnload];
}
@end
