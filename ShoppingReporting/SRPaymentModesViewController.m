//
//  SRPaymentModesPieChart.m
//  ShoppingReporting
//
//  Created by Boniface Mathieu on 5/28/13.
//  Copyright (c) 2013 Boniface Mathieu. All rights reserved.
//

#import "SRPaymentModesViewController.h"

@interface SRPaymentModesViewController ()

@property (strong, nonatomic) IBOutlet CPTGraphHostingView *hostView;
@property (nonatomic, strong) CPTPieChart *numCommandsPieChart;
@property (nonatomic, strong) NSArray *numCommandsPerPaymentMode;
@property (weak, nonatomic) IBOutlet UILabel *noDataToDisplayLabel;
@property (nonatomic, strong) NSDictionary *payementModesLabels;
@property (nonatomic, strong) NSDictionary *payementModesColors;
@property (nonatomic, strong) NSArray *availablePayementMethods;

@end

@implementation SRPaymentModesViewController

@synthesize numCommandsPieChart;
@synthesize numCommandsPerPaymentMode;
@synthesize noDataToDisplayLabel;
@synthesize hostView;
@synthesize payementModesLabels;
@synthesize payementModesColors;
@synthesize availablePayementMethods;

#pragma mark - CPTPlotDataSource methods
-(NSUInteger)numberOfRecordsForPlot:(CPTPlot *)plot {
    if (numCommandsPerPaymentMode != nil) {
        return [numCommandsPerPaymentMode count];
    }
    return 0;
}

-(void) viewDidLoad {

    availablePayementMethods = [[NSArray alloc] initWithObjects: //
                                SRAmexCardPaymentMethodKey, //
                                SRBankCardPaymentMethodKey, //
                                SRCheckPaymentMethodKey, //
                                SRGiftCardPaymentMethodKey, //
                                SRStorePaymentMethodKey, //
                                SRBuysterPaymentMethodKey, //
                                nil];
    
    payementModesLabels = [[NSDictionary alloc] initWithObjectsAndKeys://
                           @"American Express", SRAmexCardPaymentMethodKey, //
                           @"Carte Bancaire", SRBankCardPaymentMethodKey, //
                           @"Chèque", SRCheckPaymentMethodKey, //
                           @"Carte cadeaux", SRGiftCardPaymentMethodKey, //
                           @"Paiement magasin", SRStorePaymentMethodKey, //
                           @"Buyster", SRBuysterPaymentMethodKey, //
                           nil];
    
    CPTColor *amexCardColor = [[CPTColor alloc]
                               initWithComponentRed:  (float)0x6B / 0xFF // VIOLET
                               green:                 (float)0x21 / 0xFF
                               blue:                  (float)0x9E / 0xFF
                               alpha:                 (float)0xFF / 0xFF];
    CPTColor *bankCardColor = [[CPTColor alloc]
                               initWithComponentRed:  (float)0x92 / 0xFF // VERT
                               green:                 (float)0xCE / 0xFF
                               blue:                  (float)0x14 / 0xFF
                               alpha:                 (float)0xFF / 0xFF];
    CPTColor *checkColor = [[CPTColor alloc]
                            initWithComponentRed:  (float)0xB7 / 0xFF // ROUGE
                            green:                 (float)0x22 / 0xFF
                            blue:                  (float)0x1C / 0xFF
                            alpha:                 (float)0xFF / 0xFF];
    CPTColor *giftCardColor = [[CPTColor alloc]
                               initWithComponentRed:  (float)0x64 / 0xFF // ORANGE
                               green:                 (float)0x76 / 0xFF
                               blue:                  (float)0x87 / 0xFF
                               alpha:                 (float)0xFF / 0xFF];
    CPTColor *storeColor = [[CPTColor alloc]
                            initWithComponentRed:  (float)0xFA / 0xFF // COBALT / BLEU
                            green:                 (float)0x68 / 0xFF
                            blue:                  (float)0x00 / 0xFF
                            alpha:                 (float)0xFF / 0xFF];
    CPTColor *buysterColor = [[CPTColor alloc]
                              initWithComponentRed:  (float)0xE3 / 0xFF // YELLOW
                              green:                 (float)0xC8 / 0xFF
                              blue:                  (float)0x00 / 0xFF
                              alpha:                 (float)0xFF / 0xFF];
    
    payementModesColors = [[NSDictionary alloc] initWithObjectsAndKeys://
                           amexCardColor, SRAmexCardPaymentMethodKey, //
                           bankCardColor, SRBankCardPaymentMethodKey, //
                           checkColor, SRCheckPaymentMethodKey, //
                           giftCardColor, SRGiftCardPaymentMethodKey, //
                           storeColor, SRStorePaymentMethodKey, //
                           buysterColor, SRBuysterPaymentMethodKey, //
                           nil];
    [self initPlot];
}

-(NSString *)legendTitleForPieChart:(CPTPieChart *)pieChart recordIndex:(NSUInteger)index {
    return [payementModesLabels objectForKey:[availablePayementMethods objectAtIndex:index]];
}

-(CPTFill *)sliceFillForPieChart:(CPTPieChart *)pieChart recordIndex:(NSUInteger)index {
    CPTColor *color = [payementModesColors objectForKey:[availablePayementMethods objectAtIndex:index]];
    return [CPTFill fillWithColor:color];
}


-(NSNumber *)numberForPlot:(CPTPlot *)plot field:(NSUInteger)fieldEnum recordIndex:(NSUInteger)index {
    return [numCommandsPerPaymentMode objectAtIndex:index];
}

-(CPTLayer *)dataLabelForPlot:(CPTPlot *)plot recordIndex:(NSUInteger)index {

    static CPTMutableTextStyle *labelText = nil;
    double value;
    double valuesSum;

    CPTColor *color = [payementModesColors objectForKey:[availablePayementMethods objectAtIndex:index]];
    
    labelText= [[CPTMutableTextStyle alloc] init];
    labelText.color = color;
    
    NSString *labelValue = nil;
    double percent = 0;
    
    value = [[numCommandsPerPaymentMode objectAtIndex:index] doubleValue];

    valuesSum = [[numCommandsPerPaymentMode objectAtIndex:0] doubleValue]; //
    valuesSum += [[numCommandsPerPaymentMode objectAtIndex:1] doubleValue]; //
    valuesSum += [[numCommandsPerPaymentMode objectAtIndex:2] doubleValue]; //
    valuesSum += [[numCommandsPerPaymentMode objectAtIndex:3] doubleValue]; //
    if ([numCommandsPerPaymentMode count] > 5) {
        valuesSum += [[numCommandsPerPaymentMode objectAtIndex:4] doubleValue];
    }
    percent = value / valuesSum;
    
    if (index == 0) {
        double angle = (percent) * 2 * M_PI;
        double startAngle = 5 * M_PI_4 / 4 + angle / 2;
        numCommandsPieChart.startAngle = startAngle;
    }
    
    if (value != 0) {
        labelValue = [NSString stringWithFormat:@"%0.0f (%0.1f %%)", value, percent * 100.0f];
    } else {
        labelValue = @"";
    }

    CPTTextLayer *labelLayer = [[CPTTextLayer alloc] initWithText:labelValue style:labelText];
    return labelLayer;
}

#pragma mark - Rotation
-(BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    return (interfaceOrientation == UIInterfaceOrientationLandscapeLeft);
}

#pragma mark - Chart behavior
-(void)initPlot {
    [self configureHost];
    [self configureGraph];
    [self configureChart];
    [self configureLegend];
}

-(void) configureLegend {
    CPTGraph *graph = self.hostView.hostedGraph;
    CPTLegend *theLegend = [CPTLegend legendWithGraph:graph];

    theLegend.numberOfColumns = 2;

    CPTMutableTextStyle *legendTextStyle = [[CPTMutableTextStyle alloc] init];
    [legendTextStyle setColor:[CPTColor whiteColor]];

    theLegend.textStyle = legendTextStyle;
    
    theLegend.fill = [CPTFill fillWithColor:[CPTColor grayColor]];
    theLegend.cornerRadius = 5.0;

    graph.legend = theLegend;
    graph.legendAnchor = CPTRectAnchorBottom;
    graph.legendDisplacement = CGPointMake(0, 5);
}

-(void)configureHost {
    self.hostView.allowPinchScaling = NO;
}

-(void)configureGraph {
    
    CPTGraph *graph = [[CPTXYGraph alloc] init];
    self.hostView.hostedGraph = graph;
    
    [graph applyTheme:[CPTTheme themeNamed:kCPTDarkGradientTheme]];
    graph.plotAreaFrame.cornerRadius = 0;
    
    graph.fill = [CPTFill fillWithColor:[CPTColor clearColor]];
    graph.plotAreaFrame.fill = [CPTFill fillWithColor:[CPTColor clearColor]];
    
    graph.paddingBottom = 3.0f;
    graph.paddingLeft  = 3.0f;
    graph.paddingTop    = 0.0f;
    graph.paddingRight  = 3.0f;
    graph.axisSet = nil;
    
    CPTMutableTextStyle *titleStyle = [CPTMutableTextStyle textStyle];
    titleStyle.color = [CPTColor darkGrayColor];
    titleStyle.fontName = @"HelveticaNeue-Light";
    titleStyle.fontSize = 16.0f;
    titleStyle.textAlignment = CPTTextAlignmentCenter;
    
    NSString *title = @"Activité LM.fr \nRépartition des commandes \npar moyens de paiement";
    graph.title = title;
    graph.titleTextStyle = titleStyle;
    graph.titlePlotAreaFrameAnchor = CPTRectAnchorTop;
    graph.titleDisplacement = CGPointMake(0, -3.0f);
    
    graph.plotAreaFrame.borderLineStyle = nil;
    graph.plotAreaFrame.paddingBottom = 5.0f;
    graph.plotAreaFrame.paddingLeft  = 5.0f;
    graph.plotAreaFrame.paddingTop    = 0.0f;
    graph.plotAreaFrame.paddingRight  = 5.0f;
}

-(void)configureChart {
    
    CPTGraph *graph = self.hostView.hostedGraph;
    numCommandsPieChart = [[CPTPieChart alloc] init];
    numCommandsPieChart.dataSource = self;
    numCommandsPieChart.delegate = self;
    numCommandsPieChart.pieRadius = 90;
    numCommandsPieChart.sliceDirection = CPTPieDirectionClockwise;
    numCommandsPieChart.centerAnchor = CGPointMake(0.5, 0.5);
    
    CPTGradient *numCommandsOverlayGradient = [[CPTGradient alloc] init];
    numCommandsOverlayGradient.gradientType = CPTGradientTypeRadial;
    
    CPTColor *numCommandsGradientColor = [CPTColor darkGrayColor];
    
    numCommandsOverlayGradient = [numCommandsOverlayGradient addColorStop:[numCommandsGradientColor colorWithAlphaComponent:0.0] atPosition:0.5];
    numCommandsOverlayGradient = [numCommandsOverlayGradient addColorStop:[numCommandsGradientColor colorWithAlphaComponent:0.4] atPosition:1.0];
    numCommandsPieChart.overlayFill = [CPTFill fillWithGradient:numCommandsOverlayGradient];
    [graph addPlot:numCommandsPieChart];
    
}

- (void) reloadData:(NSArray *)newValues {
    numCommandsPerPaymentMode = newValues;
    
    if (newValues != nil && [newValues count] >= 4) {
        [numCommandsPieChart reloadData];
        [self configureLegend];
        
        [noDataToDisplayLabel setHidden:YES];
        [hostView setHidden:NO];

    } else {

        [self errorFetchingData];
    }
}

-(void) noData {
    [hostView setHidden:YES];
    [noDataToDisplayLabel setTextColor:[SRConstants noDataLabelColor]];
    [noDataToDisplayLabel setText:SRNoDataText];
    [noDataToDisplayLabel setHidden:NO];
}

-(void) errorFetchingData {
    [hostView setHidden:YES];
    [noDataToDisplayLabel setTextColor:[SRConstants unableToFetchDataLabelColor]];
    [noDataToDisplayLabel setText:SRUnableToFetchDataText];
    [noDataToDisplayLabel setHidden:NO];
}

- (void)viewDidUnload {
    [self setNoDataToDisplayLabel:nil];
    [super viewDidUnload];
}
@end
