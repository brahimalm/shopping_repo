//
//  SRKeyNumbersViewController.h
//  ShoppingReporting
//
//  Created by Boniface Mathieu on 5/29/13.
//  Copyright (c) 2013 Boniface Mathieu. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol  SRkeyNumbersDelegate <NSObject>
@required

-(void) onTimeOffsetChanged:(int) offset;

@end


@interface SRKeyNumbersViewController : UIViewController
{
    id <SRkeyNumbersDelegate> _delegate;
}

@property (nonatomic, assign) id <SRkeyNumbersDelegate> delegate;

-(void) reloadDataWithDataWithTurnOver:(NSString *) turnOver //
                  andNumProductsByOrder:(NSString *) numProductsByOrder //
                           andNumOrders:(NSString *) numOrders //
                     andNumProductsSold:(NSString *) numProductsSold //
                      andAmountByOrder:(NSString *) amountByOrder;

-(void) updateDateLabelWithDate:(NSDate *) date andPeriod:(int) period;

-(void) setRewardButtonEnabled:(BOOL) enabled;

-(void) setForwardButtonEnabled:(BOOL) enabled;

-(void) noData;

-(void) errorFetchingData;

@end
